//
//  Menuportadalugar.h
//  SanIsidroApp
//
//  Created by Luis on 28/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menuportadalugar : NSObject

@property(nonatomic) NSInteger codsubmenufinal;
@property(nonatomic) NSInteger numlugartipo;
@property(nonatomic) NSInteger numsubmenu;
@property(nonatomic,strong) NSString *txticono;
@property(nonatomic,strong) NSString *txtnombre;

@end
