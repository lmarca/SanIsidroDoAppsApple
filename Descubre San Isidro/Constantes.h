//
//  Constantes.h
//  CAC-ABACO
//
//  Created by Luis on 7/07/15.
//  Copyright © 2016 peru. All rights reserved.
//

#import <Foundation/Foundation.h>
#define sizeDevice [UIScreen mainScreen].bounds.size

@interface Constantes : NSObject

OBJC_EXPORT NSString *const ArrayImages;
OBJC_EXPORT NSString *const TokenApple;
OBJC_EXPORT NSString *const urlWS;
OBJC_EXPORT NSString *const textMsg;
OBJC_EXPORT NSString *const kAPIKey;
OBJC_EXPORT NSString *const access_token;

OBJC_EXPORT NSString *const ba_user_id;
OBJC_EXPORT NSString *const email;
OBJC_EXPORT NSString *const name;
OBJC_EXPORT NSString *const login_user;
OBJC_EXPORT NSString *const Sesion;
OBJC_EXPORT NSString *const isRedesSociales;
OBJC_EXPORT NSString *const kIdioma;

OBJC_EXPORT float padding;
OBJC_EXPORT float heigthButton;


@end
