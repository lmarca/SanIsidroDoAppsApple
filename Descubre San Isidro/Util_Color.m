//
//  Util_Colores.m
//  Life
//
//  Created by User on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Util_Color.h"

@implementation UIColor(ColorUtils)

+(UIColor *)colorWithHexString:(NSString *)str andAlpha:(CGFloat)alpha{
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr, NULL, 16);
    return  [UIColor colorWithHex:x andAlpha:alpha];
}

+(UIColor *)colorWithHex:(UInt32)col andAlpha:(CGFloat)alpha{
    unsigned char r, g, b;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    b = col & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:alpha];
}

@end
