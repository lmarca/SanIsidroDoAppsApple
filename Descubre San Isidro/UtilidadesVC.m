//
//  UtilidadesVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "UtilidadesVC.h"

#define urlClima @"http://api.openweathermap.org/data/2.5/weather?id=3936456&units=metric&APPID=e3ff0658920e5e4c09dadc5cfbb6a4e3&lang=es"

@interface UtilidadesVC (){
    NSString *txtimagenprincipal;
    NSString *txtimagenhistoria;
    NSMutableArray *dataSource;
    NSMutableDictionary *dictAcerca;
    
    NSString *strCompra;
    NSString *strVenta;
    NSString *strTiempo;
    NSString *strHumedad;
    NSString *strPresion;
    NSString *strTempMax;
    NSString *strTempMin;
    
    NSMutableString *strTAXIS;
    NSMutableString *strBOMBEROS;
    NSMutableString *strMUNICIPALIDAD;
    NSMutableString *strPOLICIANACIONAL;
    NSMutableString *strEMERGENCIASMEDICAS;
    NSMutableString *strCOMPANIADESEGUROS;
    NSMutableString *strSERVICIOS;
    
}

@property (nonatomic) NSString *pathURL;
@property (nonatomic) NSDictionary *parameters;

@end

@implementation UtilidadesVC

@synthesize tblUtilidades;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    strCompra = @"";
    strVenta= @"";
    strTiempo= @"";
    strHumedad= @"";
    strPresion= @"";
    strTempMax= @"";
    strTempMin= @"";
    
    dataSource = [NSMutableArray array];
    
    [self onLoadClima];
    [self onLoadTipoCambio];
    [self onLoadTAXIS];
    [self onLoadNUMEROS];
    // Do any additional setup after loading the view.
}

- (void)onLoadTAXIS{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@,40/lugaresxtipofull",urlWS) query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    strTAXIS = [NSMutableString string];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSArray *arrayJson = dictJson[@"data"];
                    if (arrayJson.count > 0) {
                        for(int i=0;i<arrayJson.count;i++)
                        {
                            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"]) {
                                [strTAXIS appendString:[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"]];
                                [strTAXIS appendString:@"\n"];
                            }
                            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtpaginaweb"]) {
                                [strTAXIS appendString:[[arrayJson objectAtIndex:i] objectForKey:@"txtpaginaweb"]];
                                [strTAXIS appendString:@"\n\n"];
                            }
                        }
                    }
                    
                    [tblUtilidades reloadData];
                }
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (void)onLoadNUMEROS{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@,42/lugaresxtipofull",urlWS) query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSArray *arrayJson = dictJson[@"data"];
                    if (arrayJson.count > 0) {
                        for(int i=0;i<arrayJson.count;i++)
                        {
                            if ([[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"] isEqualTo:@"BOMBEROS"]) {
                                strBOMBEROS = [[NSMutableString alloc]initWithString:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            }
                            if ([[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"] isEqualTo:@"MUNICIPALIDAD"]) {
                                strMUNICIPALIDAD = [[NSMutableString alloc]initWithString:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            }
                            if ([[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"] isEqualTo:@"POLICIA NACIONAL"]) {
                                strPOLICIANACIONAL = [[NSMutableString alloc]initWithString:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            }
                            if ([[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"] isEqualTo:@"URGENCIA-EMERGENCIAS MEDICAS"]) {
                                strEMERGENCIASMEDICAS = [[NSMutableString alloc]initWithString:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            }
                            if ([[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"] isEqualTo:@"COMPAÑIA DE SEGUROS "]) {
                                strCOMPANIADESEGUROS = [[NSMutableString alloc]initWithString:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            }
                            if ([[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"] isEqualTo:@"SERVICIOS"]) {
                                strSERVICIOS = [[NSMutableString alloc]initWithString:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            }
                        }
                    }
                    
                    [tblUtilidades reloadData];
                }
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellMaster = nil;
    
    if (indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"uti01";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UILabel *compra = (UILabel *)[cell viewWithTag:600];
        UILabel *venta = (UILabel *)[cell viewWithTag:601];
        
        compra.text = strCompra;
        venta.text = strVenta;

        cellMaster = cell;
    }
    
    if (indexPath.row == 1) {
        
        static NSString *CellIdentifier = @"uti02";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UILabel *tiempo = (UILabel *)[cell viewWithTag:602];
        UILabel *humedad = (UILabel *)[cell viewWithTag:603];
        UILabel *presion = (UILabel *)[cell viewWithTag:604];
        UILabel *tempMax = (UILabel *)[cell viewWithTag:605];
        UILabel *tempMin = (UILabel *)[cell viewWithTag:606];
        
        tiempo.text = strTiempo;
        humedad.text = strHumedad;
        presion.text = strPresion;
        tempMax.text = strTempMax;
        tempMin.text = strTempMin;
        
        cellMaster = cell;
    }
    if (indexPath.row == 2) {
        
        static NSString *CellIdentifier = @"uti03";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
        
    }
    if (indexPath.row == 3) {
        
        static NSString *CellIdentifier = @"uti04";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 4) {
        
        static NSString *CellIdentifier = @"uti05";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
        
    }
    if (indexPath.row == 5) {
        
        static NSString *CellIdentifier = @"uti06";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 6) {
        
        static NSString *CellIdentifier = @"uti07";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
        
    }
    if (indexPath.row == 7) {
        
        static NSString *CellIdentifier = @"uti08";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 8) {
        
        static NSString *CellIdentifier = @"uti09";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
        
    }

    
    return cellMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat ktvc03 = 0;
    
    if (indexPath.row == 0) {
        ktvc03 =  160.0f;
    }
    if (indexPath.row == 1) {
        ktvc03 =  300.0f;
    }
    if (indexPath.row == 2) {
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti03";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 3) {
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti04";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
    }
    if (indexPath.row == 4) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti05";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 5) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti06";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 6) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti07";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 7) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti08";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 8) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"uti09";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 9) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"ct09";
            cell = [tblUtilidades dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    
    return ktvc03;
}

# pragma mark - UITableViewControllerDelegate

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

# pragma mark - Cell Setup

- (void)setUpCell:(DynamicTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    [tblUtilidades setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([cell.reuseIdentifier isEqualTo:@"uti03"])
        cell.label.text = strTAXIS;
    if ([cell.reuseIdentifier isEqualTo:@"uti04"])
        cell.label.text = strBOMBEROS;
    if ([cell.reuseIdentifier isEqualTo:@"uti05"])
        cell.label.text = strMUNICIPALIDAD;
    if ([cell.reuseIdentifier isEqualTo:@"uti06"])
        cell.label.text = strPOLICIANACIONAL;
    if ([cell.reuseIdentifier isEqualTo:@"uti07"])
        cell.label.text = strEMERGENCIASMEDICAS;
    if ([cell.reuseIdentifier isEqualTo:@"uti08"])
        cell.label.text = strCOMPANIADESEGUROS;
    if ([cell.reuseIdentifier isEqualTo:@"uti09"])
        cell.label.text = strSERVICIOS;
    
    cell.label.preferredMaxLayoutWidth = self.view.frame.size.width;
}

- (void)onLoadClima{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:urlClima query:@"" state:0 withBlock:^(NSDictionary *objects, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{[self LoadingOff];});
            
            [Soporte dump:@"listado" prefijo:objects];
            
            NSDictionary *arrayJson = objects[@"main"];
            
            @try {
                if (arrayJson.count != 0) {
                    [[Memoria getInfo]setCli_temperatura:f(@"%@ °C",[arrayJson objectForKey:@"temp"])];
                    [[Memoria getInfo]setCli_humedad:f(@"%@ %@",[arrayJson objectForKey:@"humidity"],@"%")];
                    [[Memoria getInfo]setCli_tempmin:f(@"%@ °C",[arrayJson objectForKey:@"temp_min"])];
                    [[Memoria getInfo]setCli_tempmax:f(@"%@ °C",[arrayJson objectForKey:@"temp_max"])];
                    [[Memoria getInfo]setCli_temperatura:f(@"%@  hPa",[arrayJson objectForKey:@"pressure"])];
                }
            } @catch (NSException *exception) {
            }
            
            NSArray *arrayWeather = objects[@"weather"];
            
            for (NSDictionary *item in arrayWeather) {
                [[Memoria getInfo]setCli_time:item[@"description"]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                strTempMax = [Memoria getInfo].cli_tempmax;
                strTempMin = [Memoria getInfo].cli_tempmin;
                strPresion = [Memoria getInfo].cli_temperatura;
                strHumedad = [Memoria getInfo].cli_humedad;
                strTiempo = [Memoria getInfo].cli_time;
                [tblUtilidades reloadData];
            });
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
}

- (void)onLoadTipoCambio{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        NSInteger month = [components month];
        NSInteger year = [components year];
        
        NSString *urlTipoCambio = [NSString stringWithFormat:@"http://code.staffsystems.us/webservices/tipo-de-cambio/serverside.php?work=get_sunat&mes=%ld&anho=%ld",month,year];
        
        [ConnectWS findAllObjectsWithQuery:urlTipoCambio query:@"" state:0 withBlock:^(NSDictionary *objects, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{[self LoadingOff];});
            
            [Soporte dump:@"listado" prefijo:objects];
            
            NSArray *arrayJson = objects[@"data"];
            
            if (arrayJson.count != 0) {
                for (int i = 0; i<arrayJson.count; i++) {
                    [[Memoria getInfo]setCompra:[[arrayJson objectAtIndex:i] objectForKey:@"compra"]];
                    [[Memoria getInfo]setVenta:[[arrayJson objectAtIndex:i] objectForKey:@"venta"]];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                strCompra = f(@"S/. %@",[Memoria getInfo].compra);
                strVenta = f(@"S/. %@",[Memoria getInfo].venta);
                [tblUtilidades reloadData];
            });
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
