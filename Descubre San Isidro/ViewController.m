//
//  ViewController.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 29/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController (){
    IBOutlet UIImageView *imgFotoPerfil;
    IBOutlet UITableView *tbInfo;
    IBOutlet UISegmentedControl *segmentControl;
    IBOutlet UILabel *lblNombre;
    
    NSMutableArray *listaActividades;
    NSMutableArray *listaEventos;
    NSMutableArray *listaHorariosxlugar;
    
    NSMutableArray *listaEntradasxactividad;
    NSMutableArray *listaHorariosxactividad;
    NSMutableArray *listaTextopresentacionxtipoactividad;
    
    BOOL isActividad;
}

@end

@implementation ViewController

@synthesize lugar, actividad;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    lblNombre.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    
    if ([NSStandardUserDefaults boolForKey:isRedesSociales]) {
        
        if ([self verificarRutaImagenLocal:@"perfilPicture"]) {
            [imgFotoPerfil setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],@"perfilPicture"]]];
            [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
        }else {
            // Load avatar image asynchronously, in background
            dispatch_queue_t backgroundQueue =
            dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            
            dispatch_async(backgroundQueue, ^{
                NSData *avatarData = nil;
                NSString *imageURLString = [NSStandardUserDefaults stringForKey:@"userImageURL"];
                if (imageURLString) {
                    NSURL *imageURL = [NSURL URLWithString:imageURLString];
                    avatarData = [NSData dataWithContentsOfURL:imageURL];
                }
                
                if (avatarData) {
                    // Update UI from the main thread when available
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *rutaImagenLocal = [NSString stringWithFormat:@"%@/%@",[self imagePath],@"perfilPicture"];
                        if([avatarData  writeToFile:rutaImagenLocal atomically:YES]){
                            if ([self verificarRutaImagenLocal:@"perfilPicture"]) {
                                [imgFotoPerfil setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],@"perfilPicture"]]];
                                [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
                            }
                        }
                    });
                }
            });
        }
        
    }else{
        [imgFotoPerfil setImage:[UIImage imageNamed:@"placeholder_perfil"]];
    }
    
    lblNombre.text = [NSStandardUserDefaults stringForKey:@"name"];
    
    isActividad = YES;
    
    imgFotoPerfil.layer.cornerRadius = CGRectGetHeight(imgFotoPerfil.frame)/2;
    imgFotoPerfil.layer.masksToBounds = YES;
    imgFotoPerfil.layer.borderColor = [UIColor whiteColor].CGColor;
    imgFotoPerfil.layer.borderWidth = 1;
    imgFotoPerfil.contentMode = UIViewContentModeScaleAspectFill;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self llenarListaEventos];
        [self llenarListaActividades];
        [self reloadTableViewContent];
    });
}

- (IBAction)btnOnClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"APP_ALERT", nil)
                                  message:NSLocalizedString(@"APP_CERRAR_SESION", nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"APP_ACCEPT", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:Sesion];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                             
                             [[GIDSignIn sharedInstance] signOut];
                             
                             [self DeleteImagePath:[NSString stringWithFormat:@"%@/%@",[self imagePath],@"perfilPicture"]];
                             [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM LUGARES"];
                             [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM ACTIVIDADES"];
                             
                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                             LoginPrincipalVC *vc = (LoginPrincipalVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"LoginPrincipalVC"];
                             AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                             appDelegate.window.rootViewController = vc;
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (void)llenarListaEventos{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",[[Memoria sharedInstance]obtenerUserId],@"/favoritos") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSArray *arrayJson = dictJson[@"data"];
                    if (arrayJson.count > 0) {
                        [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM LUGARES"];
                        [Lugar InsertLugares:arrayJson];
                    }
                }
                [self reloadTableViewContent];
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (void)llenarListaActividades{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",[[Memoria sharedInstance]obtenerUserId],@"/misactividades") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSArray *arrayJson = dictJson[@"data"];
                    if (arrayJson.count > 0) {
                        [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM ACTIVIDADES"];
                        [Actividad InsertActividades:arrayJson];
                    }
                }
                [self reloadTableViewContent];
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (void)reloadTableViewContent {
    dispatch_async(dispatch_get_main_queue(), ^{
        listaActividades = [[NSMutableArray alloc]initWithArray:[self getActividades]];
        listaEventos = [[NSMutableArray alloc]initWithArray:[self getLugares]];
        [tbInfo reloadData];
        [tbInfo scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    });
}

- (void)deselectAllRows {
    for (NSIndexPath *indexPath in [tbInfo indexPathsForSelectedRows]) {
        [tbInfo deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (IBAction)actionSegmentIndex:(id)sender{
    isActividad = segmentControl.selectedSegmentIndex==0 ? YES : FALSE;
    listaEventos = [[NSMutableArray alloc]initWithArray:isActividad ?[self getActividades] : [self getLugares]];
    [tbInfo reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (isActividad)?listaActividades.count:listaEventos.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (isActividad)?220.0f:190.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isActividad) {
        static NSString *cellIdentifier = @"ActividadCell";
        ActividadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil) {
            cell = [[ActividadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        Actividad *ac = listaActividades[indexPath.row];
        [cell llenarCelda:ac isSucediendo:false];
        return cell;
    }
    else{
        static NSString *cellIdentifier = @"LugarCell";
        LugarCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil) {
            cell = [[LugarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        Lugar *ac = listaEventos[indexPath.row];
        [cell llenarCelda:ac];
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!isActividad) {
        lugar = listaEventos[indexPath.row];
        [self llenarDetalleLugar:lugar.codlugar];
    }else{
        actividad = listaActividades[indexPath.row];
        [self llenarDetalleActividad:actividad.codactividad];
    }
    [self deselectAllRows];
}

- (void)llenarDetalleActividad: (NSInteger)codactividad{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld,%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)codactividad,[[Memoria sharedInstance]obtenerUserId],@"/actividad|horariosxactividad|entradasxactividad|textopresentacionxtipoactividad/multiple") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSDictionary *dictActividad = dictJson[@"actividad"];
                    NSArray *arrayActividad = dictActividad[@"data"];
                    
                    if (arrayActividad.count > 0) {
                        for (int i = 0; i<arrayActividad.count; i++) {
                            actividad = [Actividad new];
                            [actividad setCodactividad:[[[arrayActividad objectAtIndex:i] objectForKey:@"codactividad"] integerValue]];
                            [actividad setNumx:[[[arrayActividad objectAtIndex:i] objectForKey:@"numx"] floatValue]];
                            [actividad setNumy:[[[arrayActividad objectAtIndex:i] objectForKey:@"numy"] floatValue]];
                            [actividad setTxtdireccion:[[arrayActividad objectAtIndex:i] objectForKey:@"txtdireccion"]];
                            [actividad setTxthorariosatencion:[[arrayActividad objectAtIndex:i] objectForKey:@"txthorariosatencion"]];
                            [actividad setTxtimagen:[[arrayActividad objectAtIndex:i] objectForKey:@"txtimagen"]];
                            [actividad setTxtnombre:[[arrayActividad objectAtIndex:i] objectForKey:@"txtnombre"]];
                            [actividad setEsfavorito:[[[arrayActividad objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
                        }
                    }
                    
                    NSDictionary *dictEntradasxactividad = dictJson[@"entradasxactividad"];
                    listaEntradasxactividad = [[NSMutableArray alloc]initWithArray:dictEntradasxactividad[@"data"]];
                    NSDictionary *dictHorariosxactividad = dictJson[@"horariosxactividad"];
                    listaHorariosxactividad = [[NSMutableArray alloc]initWithArray:dictHorariosxactividad[@"data"]];
                    NSDictionary *dictTextopresentacionxtipoactividad = dictJson[@"textopresentacionxtipoactividad"];
                    listaTextopresentacionxtipoactividad = [[NSMutableArray alloc]initWithArray:dictTextopresentacionxtipoactividad[@"data"]];
                    
                    if (listaTextopresentacionxtipoactividad.count > 0) {
                        [actividad setLogo:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"logo"]];
                        [actividad setInstitucion:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"institucion"]];
                        [actividad setTexto:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"texto"]];
                    }else{
                        [actividad setLogo:@""];
                        [actividad setInstitucion:@""];
                        [actividad setTexto:@""];
                    }
                    
                    DetalleActividadVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetalleActividadVC"];
                    vc.actividad = actividad;
                    vc.entradasxactividad = listaEntradasxactividad;
                    vc.horariosxactividad = listaHorariosxactividad;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (void)llenarDetalleLugar: (NSInteger)codlugar{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld,%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)codlugar,[[Memoria sharedInstance]obtenerUserId],@"/lugar|horariosxlugar/multiple") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSDictionary *dictLugar = dictJson[@"lugar"];
                    NSArray *arrayLugar = dictLugar[@"data"];
                    if (arrayLugar.count > 0) {
                        for (int i = 0; i<arrayLugar.count; i++) {
                            
                            lugar = [Lugar new];
                            [lugar setCodlugar:[[[arrayLugar objectAtIndex:i] objectForKey:@"codlugar"] integerValue]];
                            [lugar setNumx:[[[arrayLugar objectAtIndex:i] objectForKey:@"numx"] floatValue]];
                            [lugar setNumy:[[[arrayLugar objectAtIndex:i] objectForKey:@"numy"] floatValue]];
                            [lugar setTxtdireccion:[[arrayLugar objectAtIndex:i] objectForKey:@"txtdireccion"]];
                            [lugar setTxthorariosatencion:[[arrayLugar objectAtIndex:i] objectForKey:@"txthorariosatencion"]];
                            [lugar setTxtnombre:[[arrayLugar objectAtIndex:i] objectForKey:@"txtnombre"]];
                            [lugar setEsfavorito:[[[arrayLugar objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
                            
                            if ([[arrayLugar objectAtIndex:i] objectForKey:@"txtimagen"] == nil)
                                [lugar setTxtimagen:@""];
                            else
                                [lugar setTxtimagen:[[arrayLugar objectAtIndex:i] objectForKey:@"txtimagen"]];
                            
                            if ([[arrayLugar objectAtIndex:i] objectForKey:@"txtdescripcion"] == nil)
                                [lugar setTxtdescripcion:@""];
                            else
                                [lugar setTxtdescripcion:[[arrayLugar objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                            
                            if ([[arrayLugar objectAtIndex:i] objectForKey:@"txttelefono"] == nil)
                                [lugar setTxttelefono:@""];
                            else
                                [lugar setTxttelefono:[[arrayLugar objectAtIndex:i] objectForKey:@"txttelefono"]];
                            
                            if ([[arrayLugar objectAtIndex:i] objectForKey:@"txtpaginaweb"] == nil)
                                [lugar setTxtpaginaweb:@""];
                            else
                                [lugar setTxtpaginaweb:[[arrayLugar objectAtIndex:i] objectForKey:@"txtpaginaweb"]];
                            
                        }
                    }
                    
                    NSDictionary *dictHorariosxlugar = dictJson[@"horariosxlugar"];
                    listaHorariosxlugar = [[NSMutableArray alloc]initWithArray:dictHorariosxlugar[@"data"]];
                    
                    DetalleLugarVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetalleLugarVC"];
                    vc.lugar = lugar;
                    vc.listaHorariosxlugar = listaHorariosxlugar;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (NSMutableArray *)getActividades{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSArray *array = [[SyncronizeData sharedInstance]loadDataFromDB:@"SELECT * FROM ACTIVIDADES"];
    for (NSMutableArray *lista in array) {
        Actividad *objActividad = [[Actividad alloc] init];
        
        objActividad.codactividad = [lista[1]integerValue];
        objActividad.institucion = lista[2];
        objActividad.numdia = [lista[3]integerValue];
        objActividad.numesactividad = [lista[4]integerValue];
        objActividad.nummes = [lista[5]integerValue];
        objActividad.txtdireccion = lista[6];
        objActividad.txtfechaevento = lista[7];
        objActividad.txtimagen = lista[8];
        objActividad.txtnombre = lista[9];
        
        [list addObject:objActividad];
        objActividad = nil;
    }
    return list;
}

- (NSMutableArray *)getLugares{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSArray *array = [[SyncronizeData sharedInstance]loadDataFromDB:@"SELECT * FROM LUGARES"];
    for (NSMutableArray *lista in array) {
        
        Lugar *objLugar = [[Lugar alloc] init];
        
        objLugar.codlugar = [lista[1]integerValue];
        objLugar.numx = [lista[2]floatValue];
        objLugar.numy = [lista[3]floatValue];
        objLugar.txtdescripcion = lista[4];
        objLugar.txtdireccion = lista[5];
        objLugar.txthorariosatencion = lista[6];
        objLugar.txtimagen = lista[7];
        objLugar.txtnombre = lista[8];
        objLugar.txttelefono = lista[9];
        objLugar.txtpaginaweb = lista[10];
        
        [list addObject:objLugar];
        objLugar = nil;
    }
    return list;
}


@end
