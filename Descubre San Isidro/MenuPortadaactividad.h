//
//  MenuPortadaactividad.h
//  Descubre San Isidro
//
//  Created by Luis on 28/06/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuPortadaactividad : NSObject

@property(nonatomic) NSInteger numsubmenu;
@property(nonatomic,strong) NSString *txtnombre;
@property(nonatomic) NSInteger codactividadtipo;
@property(nonatomic) NSInteger codsubmenufinal;
@property(nonatomic,strong) NSString *txticono;


@end
