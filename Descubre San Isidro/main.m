//
//  main.m
//  Descubre San Isidro
//
//  Created by Luis on 30/08/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
