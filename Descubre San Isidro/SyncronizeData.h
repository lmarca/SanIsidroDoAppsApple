//
//  SyncronizeData.h
//  SanIsidroApp
//
//  Created by DoApps on 26/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"

@interface SyncronizeData : NSObject{
    NSURLConnection* connection;
    NSMutableData* data;
}

@property(nonatomic,strong) DBManager *dbManager;

+ (SyncronizeData *) sharedInstance;

- (void)executeQuery:(NSString *)query;
- (NSArray *)loadDataFromDB:(NSString *)query;

@end
