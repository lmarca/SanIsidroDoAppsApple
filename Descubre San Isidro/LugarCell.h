//
//  LugarCell.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lugar.h"
#import "AsyncImageView.h"

@interface LugarCell : UITableViewCell{
    
    IBOutlet AsyncImageView *imgBanner;
    IBOutlet UILabel *lblNombre;
    IBOutlet UILabel *lblDireccion;
}

-(void)llenarCelda:(Lugar *)lugar;

@end
