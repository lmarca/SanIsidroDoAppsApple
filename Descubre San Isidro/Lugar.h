//
//  Lugar.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncronizeData.h"

@interface Lugar : NSObject

@property(nonatomic) NSInteger codlugar;
@property(nonatomic) float numx;
@property(nonatomic) float numy;
@property(nonatomic,strong) NSString *txtdescripcion;
@property(nonatomic,strong) NSString *txtdireccion;
@property(nonatomic,strong) NSString *txthorariosatencion;
@property(nonatomic,strong) NSString *txtimagen;
@property(nonatomic,strong) NSString *txtnombre;
@property(nonatomic,strong) NSString *txtpaginaweb;
@property(nonatomic,strong) NSString *txttelefono;
@property(nonatomic) NSInteger esfavorito;

+ (void)InsertLugares:(NSArray *)arrayJson;

@end
