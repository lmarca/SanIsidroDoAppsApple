//
//  CicloViaVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "CicloViaVC.h"

@interface CicloViaVC (){
    BOOL firstLocationUpdate_;
}

@end

@implementation CicloViaVC

- (NSString *)deviceLat {
    //metodo que devuelve la latitud del usuario
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    //metodo que devuelve la longitud del usuario
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if(IS_OS_8_OR_LATER) {
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [_locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [_locationManager  requestWhenInUseAuthorization];
            } else {
                [Soporte dump:@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription"];
            }
        }
    }
    [_locationManager startUpdatingLocation];
    
    self.locationManager.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    
    NSString *latitud = self.deviceLat;
    NSString *longitud = self.deviceLon;
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        _map.myLocationEnabled = YES;
    });
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[latitud floatValue]
                                         longitude:[longitud floatValue]
                                              zoom:kZoom
                                           bearing:328.f
                                      viewingAngle:40.f];
    
    _map.camera = camera;
    _map.settings.compassButton = YES;
    _map.settings.myLocationButton = YES;
    _map.delegate = self;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([latitud floatValue], [longitud floatValue]);
    //marker.snippet = actividad.txtnombre;
    marker.icon = [UIImage imageNamed:@"location"];
    marker.draggable = NO;
    marker.map = _map;
    [_map setSelectedMarker:marker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
