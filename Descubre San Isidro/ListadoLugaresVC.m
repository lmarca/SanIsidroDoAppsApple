//
//  ListadoLugaresVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 4/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "ListadoLugaresVC.h"

@interface ListadoLugaresVC (){
    IBOutlet UILabel *lblCabecera;
    IBOutlet UITableView *tbInfo;
    NSMutableArray *listaEventos;
}

@end

@implementation ListadoLugaresVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //lblCabecera.text = _lugar.nombre;
    [self llenarListaEventos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)llenarListaEventos{
    listaEventos = [[NSMutableArray alloc] init];
    for (NSInteger i=0; i<3; i++) {
        Evento *ac = [[Evento alloc] init];
        ac.creador = @"Centro cultural El Olivar";
        ac.direccion = @"Calle la República 455";
        ac.foto = @"img_evento02";
        [listaEventos addObject:ac];
    }
    
    [tbInfo reloadData];
}

#pragma mark - IBActions

-(IBAction)btnBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listaEventos.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellIdentifier = @"EventoCell";
    EventoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[EventoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    Evento *ac = listaEventos[indexPath.row];
    [cell llenarCelda:ac];
    return cell;
}

@end
