//
//  Soporte.m
//  Demo
//
//  Created by Procesos on 2/02/15.
//  Copyright (c) 2015 com.procesosmc. All rights reserved.
//

#import "Soporte.h"

@implementation Soporte

/**
 * Clase utilitaria que permite manejar diversos métodos utilizados en el sistema movil
 * @author Luis Marca
 * @version 1.0
 */


/************************************************************************
* @funciones numericas
*/

+ (int )toInt:(NSString *)valor
{
    int intValue = 0;
    @try {
        if (![self isNullOrE:valor]) {
            intValue = [valor intValue];
        }
    }
    @catch (NSException *exception) {
    }
    return intValue;
}

+ (short )toShort:(NSString *)valor
{
    short intValue = 0;
    @try {
        if (![self isNullOrE:valor]) {
            intValue = [valor intValue];
            intValue = (unsigned short)valor;
            //unsigned short sin_port = (unsigned short)valor;
        }
    }
    @catch (NSException *exception) {
    }
    return intValue;
}

+ (double )toDouble:(NSString *)valor
{
    double retorno = 0;
    @try {
        if (![self isNullOrE:valor]) {
            retorno = [valor doubleValue];
        }
    }
    @catch (NSException *exception) {
    }
    return retorno;
}

+ (void)dump:(NSString *)prefijo exception:(NSException *)e
{
    [self dump:[NSString stringWithFormat:@"%@ : %@",prefijo,e.description]];
}

+ (void)dump:(NSString *)prefijo E:(NSString *)e
{
    [self dump:[NSString stringWithFormat:@"%@ : %@",prefijo,e]];
}

+ (void)dump:(NSString *)message prefijo:(id )e
{
    [self dump:[NSString stringWithFormat:@"%@ : %@",message,e]];
}

+ (void)dump:(NSString *)mensaje
{
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    [format setDateFormat:@"HH:mm:ss"];
    NSString *fechaHora = [format stringFromDate:[NSDate date]];
    //NSLog(@"debug %@",mensaje);
    format = nil;
    NSLog(@"%@ | %@",fechaHora,mensaje);
}

+ (void)alert:(NSString *)mensaje
{
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    [format setDateFormat:@"HH:mm:ss"];
    NSString *fechaHora = [format stringFromDate:[NSDate date]];
    format = nil;
    NSLog(@"%@ | %@",fechaHora,mensaje);
}

+ (void)err:(NSException *)mensaje
{
    [self dump:mensaje.description];
}

+ (BOOL)isNullOrE:(NSString *)texto
{
    return [self isNullOrEmpty:texto];
}

+ (BOOL)isNullOrEmpty:(NSString *)texto
{
    return texto == NULL || texto.length == 0;
}

+ (BOOL) validEmail:(NSString*)emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return false;
    } else
        return true;
}

+ (BOOL )listMatches: (NSString *)pattern stringT:(NSString *)string
{
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionIgnoreMetacharacters error:nil];
    NSRange range = NSMakeRange(0, [string length]);
    NSUInteger regExMatches = [regEx numberOfMatchesInString:string options:0 range:range];
    if (regExMatches == 0) {
        return false;
    }else{
        return true;
    }
}

+ (BOOL) validEmail:(NSString*)emailString regExPattern:(NSString *)regExPattern{
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return false;
    } else
        return true;
}

+ (BOOL)validaTelefono:(NSString *)numero
{
    int valor = [numero intValue];
    if (valor>100000000 && valor<=999999999)
        return true;
    else
        return false;
}

+ (NSArray*)regexToArray:(NSString*) regexPattern string:(NSString *)string {
    
    NSMutableArray *data = [NSMutableArray new];
    
    @try {
        
        NSRegularExpression *regEx = [[NSRegularExpression alloc]
                                      initWithPattern:regexPattern
                                      options:NSRegularExpressionCaseInsensitive
                                      error:nil];
        
        NSArray *matches = [regEx matchesInString:string options:0 range:NSMakeRange(0, string.length)];
        for( NSTextCheckingResult* match in matches) {
            NSString* item = [string substringWithRange:match.range];
            //NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            //[data addObject:[[item componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""]];
            [data addObject:item];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"error %@",exception.description);
    }
    
    return data;
}

+ (BOOL)isGmail:(NSString *)email
{
    return [email hasSuffix:@"gmail.com"];
}

+ (BOOL)isYahoo:(NSString *)email
{
    return [email hasSuffix:@"yahoo.com"] ||
    [email hasSuffix:@"yahoo.es"] ||
    [email hasSuffix:@"ymail.com"] ||
    [email hasSuffix:@"rocketmail.com"]
    ;
}


@end
