//
//  ListadoActividadesVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 4/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "ListadoActividadesVC.h"

@interface ListadoActividadesVC (){
    IBOutlet UILabel *lblTitulo;
    IBOutlet UITableView *tbInfo;
    
    NSMutableArray *dataSource;
    NSMutableArray *listaHorariosxlugar;
    
    NSMutableArray *listaEntradasxactividad;
    NSMutableArray *listaHorariosxactividad;
    NSMutableArray *listaTextopresentacionxtipoactividad;
    
    BOOL isSearching;
}

@end

@implementation ListadoActividadesVC

@synthesize objMenuActividad, isActividad, vTopBar, objMenu, tblGenerico, search, lugar, actividad;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    vTopBar.backgroundColor = [UIColor colorWithHexString:isActividad ? @"9E0053" : @"6F9C4D" andAlpha:1.0];
    
    lblTitulo.text = isActividad ? objMenuActividad.txtnombre : objMenu.txtnombre;
    
    if (isActividad) {
        
        [self llenarActividadesxCategoria];
        
    }else{
        [self llenarListaLugares];
    }
    
    //search.barTintColor = [UIColor colorWithHexString:isActividad ? @"9E0053" : @"6F9C4D" andAlpha:1.0];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [Soporte dump:searchText];
    
    if([searchText length] != 0) {
        isSearching = YES;
        if (isActividad)
            dataSource = [[NSMutableArray alloc]initWithArray:[self getActividades:true filter:searchText]];
        else
            dataSource = [[NSMutableArray alloc]initWithArray:[self getLugares:true filter:searchText]];
        
    }
    else{
        [searchBar resignFirstResponder];
        isSearching = NO;
        
        if (isActividad)
            dataSource = [[NSMutableArray alloc]initWithArray:[self getActividades:false filter:@""]];
        else
            dataSource = [[NSMutableArray alloc]initWithArray:[self getLugares:false filter:@""]];
    }
    [tblGenerico reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [Soporte dump:@"Cancel clicked"];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [Soporte dump:@"Search Clicked"];
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)llenarActividadesxCategoria{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)objMenuActividad.codactividadtipo,@"/actividadesxtipo") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM ACTIVIDADES"];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    NSArray *arrayJson = dictJson[@"data"];
                    if (arrayJson.count > 0) {
                        [Actividad InsertActividades:arrayJson];
                    }
                    
                    dataSource = [[NSMutableArray alloc]initWithArray:[self getActividades:false filter:@""]];
                    [tblGenerico reloadData];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

//'%computer%'
- (NSMutableArray *)getActividades:(BOOL)isFilter filter:(NSString *)filter{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSArray *array = [[SyncronizeData sharedInstance]loadDataFromDB:isFilter?f(@"SELECT * FROM ACTIVIDADES WHERE txtnombre LIKE '%%%@%%'",filter): @"SELECT * FROM ACTIVIDADES"];
    for (NSMutableArray *lista in array) {
        Actividad *objActividad = [[Actividad alloc] init];
        
        objActividad.codactividad = [lista[1]integerValue];
        objActividad.institucion = lista[2];
        objActividad.numdia = [lista[3]integerValue];
        objActividad.numesactividad = [lista[4]integerValue];
        objActividad.nummes = [lista[5]integerValue];
        objActividad.txtdireccion = lista[6];
        objActividad.txtfechaevento = lista[7];
        objActividad.txtimagen = lista[8];
        objActividad.txtnombre = lista[9];
        
        [list addObject:objActividad];
        objActividad = nil;
    }
    return list;
}

- (void)llenarListaLugares{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)objMenu.numlugartipo,@"/lugaresxtipo") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    NSArray *arrayJson = dictJson[@"data"];
                    if (arrayJson.count > 0) {
                        [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM LUGARES"];
                        [Lugar InsertLugares:arrayJson];
                    }
            
                    dataSource = [[NSMutableArray alloc]initWithArray:[self getLugares:false filter:@""]];
                    [tblGenerico reloadData];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (NSMutableArray *)getLugares:(BOOL)isFilter filter:(NSString *)filter{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSArray *array = [[SyncronizeData sharedInstance]loadDataFromDB:isFilter?f(@"SELECT * FROM LUGARES WHERE txtnombre LIKE '%%%@%%'",filter): @"SELECT * FROM LUGARES"];
    for (NSMutableArray *lista in array) {
        
        Lugar *objLugar = [[Lugar alloc] init];
        
        objLugar.codlugar = [lista[1]integerValue];
        objLugar.numx = [lista[2]floatValue];
        objLugar.numy = [lista[3]floatValue];
        objLugar.txtdescripcion = lista[4];
        objLugar.txtdireccion = lista[5];
        objLugar.txthorariosatencion = lista[6];
        objLugar.txtimagen = lista[7];
        objLugar.txtnombre = lista[8];
        objLugar.txttelefono = lista[9];
        objLugar.txtpaginaweb = lista[10];
        
        [list addObject:objLugar];
        objLugar = nil;
    }
    return list;
}

#pragma mark - IBActions

- (IBAction)btnBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (isActividad)?220.0f:190.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isActividad) {
        static NSString *cellIdentifier = @"ActividadCell";
        ActividadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil) {
            cell = [[ActividadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        Actividad *ac = dataSource[indexPath.row];
        
        [cell llenarCelda:ac isSucediendo:false];
        return cell;
    }
    else{
        static NSString *cellIdentifier = @"LugarCell";
        LugarCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil) {
            cell = [[LugarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        Lugar *ac = dataSource[indexPath.row];
        [cell llenarCelda:ac];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (!isActividad) {
        lugar = dataSource[indexPath.row];
        [self llenarDetalleLugar:lugar.codlugar];
    }else{
        actividad = dataSource[indexPath.row];
        [self llenarDetalleActividad:actividad.codactividad];
    }
}

- (void)llenarDetalleLugar: (NSInteger)codlugar{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld,%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)codlugar,[[Memoria sharedInstance]obtenerUserId],@"/lugar|horariosxlugar/multiple") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSDictionary *dictLugar = dictJson[@"lugar"];
                    NSArray *arrayLugar = dictLugar[@"data"];
                    
                    for (int i = 0; i<arrayLugar.count; i++) {
                        
                        lugar = [Lugar new];
                        [lugar setCodlugar:[[[arrayLugar objectAtIndex:i] objectForKey:@"codlugar"] integerValue]];
                        [lugar setNumx:[[[arrayLugar objectAtIndex:i] objectForKey:@"numx"] floatValue]];
                        [lugar setNumy:[[[arrayLugar objectAtIndex:i] objectForKey:@"numy"] floatValue]];
                        [lugar setTxtdireccion:[[arrayLugar objectAtIndex:i] objectForKey:@"txtdireccion"]];
                        [lugar setTxthorariosatencion:[[arrayLugar objectAtIndex:i] objectForKey:@"txthorariosatencion"]];
                        [lugar setTxtnombre:[[arrayLugar objectAtIndex:i] objectForKey:@"txtnombre"]];
                        [lugar setEsfavorito:[[[arrayLugar objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
                        
                        if ([[arrayLugar objectAtIndex:i] objectForKey:@"txtimagen"] == nil)
                            [lugar setTxtimagen:@""];
                        else
                            [lugar setTxtimagen:[[arrayLugar objectAtIndex:i] objectForKey:@"txtimagen"]];
                        
                        if ([[arrayLugar objectAtIndex:i] objectForKey:@"txtdescripcion"] == nil)
                            [lugar setTxtdescripcion:@""];
                        else
                            [lugar setTxtdescripcion:[[arrayLugar objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                        
                        if ([[arrayLugar objectAtIndex:i] objectForKey:@"txttelefono"] == nil)
                            [lugar setTxttelefono:@""];
                        else
                            [lugar setTxttelefono:[[arrayLugar objectAtIndex:i] objectForKey:@"txttelefono"]];
                        
                        if ([[arrayLugar objectAtIndex:i] objectForKey:@"txtpaginaweb"] == nil)
                            [lugar setTxtpaginaweb:@""];
                        else
                            [lugar setTxtpaginaweb:[[arrayLugar objectAtIndex:i] objectForKey:@"txtpaginaweb"]];
                        
                    }
                    
                    NSDictionary *dictHorariosxlugar = dictJson[@"horariosxlugar"];
                    listaHorariosxlugar = [[NSMutableArray alloc]initWithArray:dictHorariosxlugar[@"data"]];
                    
                    DetalleLugarVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetalleLugarVC"];
                    vc.lugar = lugar;
                    vc.listaHorariosxlugar = listaHorariosxlugar;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (void)llenarDetalleActividad: (NSInteger)codactividad{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld,%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)codactividad,[[Memoria sharedInstance]obtenerUserId],@"/actividad|horariosxactividad|entradasxactividad|textopresentacionxtipoactividad/multiple") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSDictionary *dictActividad = dictJson[@"actividad"];
                    NSArray *arrayActividad = dictActividad[@"data"];
                    
                    for (int i = 0; i<arrayActividad.count; i++) {
                        actividad = [Actividad new];
                        [actividad setCodactividad:[[[arrayActividad objectAtIndex:i] objectForKey:@"codactividad"] integerValue]];
                        [actividad setNumx:[[[arrayActividad objectAtIndex:i] objectForKey:@"numx"] floatValue]];
                        [actividad setNumy:[[[arrayActividad objectAtIndex:i] objectForKey:@"numy"] floatValue]];
                        [actividad setTxtdescripcion:[[arrayActividad objectAtIndex:i] objectForKey:@"txtdescripcion"]];
                        [actividad setTxtdireccion:[[arrayActividad objectAtIndex:i] objectForKey:@"txtdireccion"]];
                        [actividad setTxthorariosatencion:[[arrayActividad objectAtIndex:i] objectForKey:@"txthorariosatencion"]];
                        [actividad setTxtimagen:[[arrayActividad objectAtIndex:i] objectForKey:@"txtimagen"]];
                        [actividad setTxtnombre:[[arrayActividad objectAtIndex:i] objectForKey:@"txtnombre"]];
                        [actividad setEsfavorito:[[[arrayActividad objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
                        
                        [actividad setEstacionamientodiscapacitado:[[[arrayActividad objectAtIndex:i] objectForKey:@"estacionamientodiscapacitado"] integerValue]];
                        [actividad setEstacionamientobicicleta:[[[arrayActividad objectAtIndex:i] objectForKey:@"estacionamientobicicleta"] integerValue]];
                    }
                    
                    NSDictionary *dictEntradasxactividad = dictJson[@"entradasxactividad"];
                    listaEntradasxactividad = [[NSMutableArray alloc]initWithArray:dictEntradasxactividad[@"data"]];
                    NSDictionary *dictHorariosxactividad = dictJson[@"horariosxactividad"];
                    listaHorariosxactividad = [[NSMutableArray alloc]initWithArray:dictHorariosxactividad[@"data"]];
                    NSDictionary *dictTextopresentacionxtipoactividad = dictJson[@"textopresentacionxtipoactividad"];
                    listaTextopresentacionxtipoactividad = [[NSMutableArray alloc]initWithArray:dictTextopresentacionxtipoactividad[@"data"]];
                    
                    if (listaTextopresentacionxtipoactividad.count > 0) {
                        [actividad setLogo:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"logo"]];
                        [actividad setInstitucion:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"institucion"]];
                        [actividad setTexto:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"texto"]];
                    }else{
                        [actividad setLogo:@""];
                        [actividad setInstitucion:@""];
                        [actividad setTexto:@""];
                    }
                    
                    DetalleActividadVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetalleActividadVC"];
                    vc.actividad = actividad;
                    vc.entradasxactividad = listaEntradasxactividad;
                    vc.horariosxactividad = listaHorariosxactividad;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}


@end
