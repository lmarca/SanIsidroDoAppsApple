//
//  InfoUtilidadE.h
//  Turismo San Isidro
//
//  Created by Luis on 20/10/15.
//  Copyright © 2015 DoApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoUtilidadE : NSObject

+ (InfoUtilidadE *) sharedInstance;

@property(nonatomic,strong)  NSString *compra;
@property(nonatomic,strong)  NSString *venta;

@property(nonatomic,strong)  NSString *cli_fecha;
@property(nonatomic,strong)  NSString *cli_time;
@property(nonatomic,strong)  NSString *cli_cielo;
@property(nonatomic,strong)  NSString *cli_humedad;
@property(nonatomic,strong)  NSString *cli_presion;
@property(nonatomic,strong)  NSString *cli_tempmax;
@property(nonatomic,strong)  NSString *cli_tempmin;
@property(nonatomic,strong)  NSString *cli_temperatura;

@end
