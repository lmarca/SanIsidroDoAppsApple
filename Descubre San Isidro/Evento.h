//
//  Evento.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Evento : NSObject

@property(nonatomic,strong) NSString *Id;
@property(nonatomic,strong) NSString *nombre;
@property(nonatomic,strong) NSString *fecha;
@property(nonatomic,strong) NSString *creador;
@property(nonatomic,strong) NSString *foto;
@property(nonatomic,strong) NSString *direccion;

@end
