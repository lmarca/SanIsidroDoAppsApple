//
//  SyncronizeData.h
//  SanIsidroApp
//
//  Created by DoApps on 26/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "SyncronizeData.h"
#import "Memoria.h"
#import "Utiles.h"
#import "Soporte.h"

@implementation SyncronizeData

@synthesize dbManager = _dbManager;

+ (SyncronizeData *)sharedInstance {
    static SyncronizeData *_sharedSingleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedSingleton = [[self alloc] init];
    });
    
    return _sharedSingleton;
}

- (id) init{
    if (self = [super init])
    {
        [Soporte dump:@"Iniciando BD"];
        _dbManager = [[DBManager alloc] initWithDatabaseFilename:@"SanIsidro.sqlite"];
    }
    
    return self;
}

- (void)executeQuery:(NSString *)query{
    [Soporte dump:query];
    [_dbManager executeQuery:query];
}

- (NSArray *)loadDataFromDB:(NSString *)query{
    [Soporte dump:query];
    NSArray *list = [_dbManager loadDataFromDB:query];
    // Returned the loaded results.
    return (NSArray *)list;
}


@end
