//
//  DetalleLugarVC.h
//  SanIsidroApp
//
//  Created by Luis on 1/05/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myActivity.h"
#import "Lugar.h"
#import "DynamicTableViewCell.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface DetalleLugarVC : myActivity <GMSMapViewDelegate, UIActionSheetDelegate,UIDocumentInteractionControllerDelegate>
{
    SLComposeViewController *mySLComposerSheet;
}

@property (retain) UIDocumentInteractionController * documentInteractionController;
@property(nonatomic,strong) Lugar *lugar;
@property(nonatomic,strong) NSMutableArray *listaHorariosxlugar;

@property (strong, nonatomic) IBOutlet UITableView *tblDetalleLugar;
@property (weak, nonatomic) IBOutlet UIButton *button;




@end
