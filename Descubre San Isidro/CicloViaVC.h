//
//  CicloViaVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "myActivity.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)//constante para definir version iOS porque a partir de la 8.0, el procedimiento de posicionamiento GPS del usuario en el mapa es diferente

@interface CicloViaVC : myActivity<GMSMapViewDelegate,CLLocationManagerDelegate>{
    UIButton *btnLocation;
    UIButton *btnZoomIn, *btnZoomOut;
    UIView *contenedor;
}

@property (weak, nonatomic) IBOutlet GMSMapView *map;
@property (nonatomic, assign)CLLocationCoordinate2D  puntoOrigen;
@property (nonatomic, assign)CLLocationCoordinate2D puntoDestino;
@property(nonatomic)CLLocationManager *locationManager;

@end
