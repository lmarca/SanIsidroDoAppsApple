//
//  SanIsidroVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "SanIsidroVC.h"

#define kFieldFontSize 4

@interface SanIsidroVC (){
    
    IBOutlet UITableView *tbDatos;
    NSString *txtimagenprincipal;
    NSString *txtimagenhistoria;
    NSMutableArray *dataSource;
    NSMutableDictionary *dictAcerca;
    CGFloat HEIGHT_WEBVIEW;
    
}


@property (nonatomic) NSString *pathURL;
@property (nonatomic) NSDictionary *parameters;

@end

@implementation SanIsidroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HEIGHT_WEBVIEW = 0.0;
    
    dispatch_async(dispatch_get_main_queue(), ^{
            dataSource = [NSMutableArray array];
            dictAcerca = [NSMutableDictionary dictionary];
            [self cargarAcercaDe];
    });
    
}

- (void)cargarAcercaDe{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",@"17/submenu|contenidosxsubmenu/multiple") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    NSDictionary *dictsubmenu = dictJson[@"submenu"];
                    for (NSDictionary *dic in dictsubmenu[@"data"]) {
                        txtimagenprincipal = dic[@"txtimagenprincipal"];
                    }
                    
                    NSDictionary *dictcontenidosxsubmenu = dictJson[@"contenidosxsubmenu"];
                    for (NSDictionary *dic in dictcontenidosxsubmenu[@"data"]) {
                        
                        [dictAcerca setValue:dic[@"txttexto"] forKey:dic[@"txtetiqueta"]];
                        
                        if (txtimagenhistoria.length == 0) {
                            txtimagenhistoria = dic[@"txtimagen"];
                        }
                    }
                    
                    [tbDatos reloadData];
                }
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (HEIGHT_WEBVIEW == 0.0) {
        HEIGHT_WEBVIEW = [[webView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue] + 240;
        [tbDatos reloadData];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellMaster = nil;
    
    if (indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"ct01";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        AsyncImageView *thumb = (AsyncImageView *)[cell viewWithTag:80];
        [thumb setBackgroundColor:[UIColor lightGrayColor]];
        
        NSString *img = txtimagenprincipal;
        
        if (txtimagenprincipal != NULL) {
            img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
            
            if ([self verificarRutaImagenLocal:img])
                [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
            else
                [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,txtimagenprincipal]];
        }
        
        cellMaster = cell;
    }
    
    if (indexPath.row == 1) {
        
        static NSString *CellIdentifier = @"ct02";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIWebView *webView = (UIWebView *)[cell viewWithTag:93];
        webView.delegate = self;
        //webView.scrollView.scrollEnabled = NO;
        //webView.scrollView.bounces = NO;// not scrolled uiwebview!
        //webView.scalesPageToFit = TRUE;
        
        if (dictAcerca[[Memoria sharedInstance].isEspanol? @"Ciudad" : @"City"]) {
            NSString *strDataFromHTML = dictAcerca[[Memoria sharedInstance].isEspanol? @"Ciudad" : @"City"];
            
            [webView loadHTMLString:[NSString stringWithFormat:@"<html><body style=\"background-color: white; font-size:6px; font-family: HelveticaNeue; color: #555555\">%@</body></html>", strDataFromHTML] baseURL: nil];
        }
        
        cellMaster = cell;
    }
    if (indexPath.row == 2) {
        
        static NSString *CellIdentifier = @"ct03";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
        
    }
    if (indexPath.row == 3) {
        
        static NSString *CellIdentifier = @"ct04";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 4) {
        
        static NSString *CellIdentifier = @"ct05";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        AsyncImageView *thumb = (AsyncImageView *)[cell viewWithTag:81];
        [thumb setBackgroundColor:[UIColor lightGrayColor]];
        
        NSString *img = txtimagenhistoria;
        
        if (txtimagenhistoria != NULL) {
            img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
            
            if ([self verificarRutaImagenLocal:img])
                [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
            else
                [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,txtimagenhistoria]];
        }
        
        cellMaster = cell;
    }
    if (indexPath.row == 5) {
        
        static NSString *CellIdentifier = @"ct06";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIWebView *webView = (UIWebView *)[cell viewWithTag:82];
        
        if (dictAcerca[[Memoria sharedInstance].isEspanol? @"Datos de Contacto" : @"Contact information"]) {
            NSString *strDataFromHTML = dictAcerca[[Memoria sharedInstance].isEspanol? @"Datos de Contacto" : @"Contact information"];
            webView.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
            
            [webView loadHTMLString:[NSString stringWithFormat:@"<html><body style=\"background-color: white; font-size:6px; font-family: HelveticaNeue; color: #555555\">%@</body></html>", strDataFromHTML] baseURL: nil];
        }
        
        cellMaster = cell;
    }
    
    return cellMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat ktvc03 = 0;
    
    if (indexPath.row == 0) {
        ktvc03 =  180.0f;
    }
    if (indexPath.row == 1) {
        ktvc03 = HEIGHT_WEBVIEW;
    }
    if (indexPath.row == 2) {
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"ct03";
            cell = [tbDatos dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
    }
    if (indexPath.row == 3) {
        
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"ct04";
            cell = [tbDatos dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        
    }
    if (indexPath.row == 4) {
        ktvc03 =  180.0f;
    }
    if (indexPath.row == 5) {
        ktvc03 =  200.0f;
    }
    
    return ktvc03;
}

# pragma mark - UITableViewControllerDelegate

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 20;
}

# pragma mark - Cell Setup

- (void)setUpCell:(DynamicTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    [tbDatos setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([cell.reuseIdentifier isEqualTo:@"ct03"])
        cell.label.text = dictAcerca[[Memoria sharedInstance].isEspanol? @"Cultura" : @"Culture"];
        if ([cell.reuseIdentifier isEqualTo:@"ct04"])
            cell.label.text = dictAcerca[[Memoria sharedInstance].isEspanol? @"Historia" : @"History"];
    
    cell.label.preferredMaxLayoutWidth = self.view.frame.size.width;
}


@end
