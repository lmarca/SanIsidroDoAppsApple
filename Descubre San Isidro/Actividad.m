//
//  Actividad.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "Actividad.h"

@implementation Actividad

+ (void)InsertActividades:(NSArray *)arrayJson{
    
    [[SyncronizeData sharedInstance]executeQuery:@"DELETE FROM ACTIVIDADES"];
    
    if (arrayJson.count != 0){
        //GUARDAMOS EN LA BD
        for (int i = 0; i<arrayJson.count; i++) {
            
            Actividad *ObjActividad = [Actividad new];
            [ObjActividad setCodactividad:[[[arrayJson objectAtIndex:i] objectForKey:@"codactividad"] integerValue]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtinstitucion"] == nil){
                if ([[arrayJson objectAtIndex:i] objectForKey:@"institucion"] == nil)
                    [ObjActividad setInstitucion:@""];
                else
                    [ObjActividad setInstitucion:[[arrayJson objectAtIndex:i] objectForKey:@"institucion"]];
            }else
                [ObjActividad setInstitucion:[[arrayJson objectAtIndex:i] objectForKey:@"txtinstitucion"]];
            
            [ObjActividad setNumdia:[[[arrayJson objectAtIndex:i] objectForKey:@"numdia"] integerValue]];
            [ObjActividad setNumesactividad:[[[arrayJson objectAtIndex:i] objectForKey:@"numesactividad"] integerValue]];
            [ObjActividad setNummes:[[[arrayJson objectAtIndex:i] objectForKey:@"nummes"] integerValue]];
            
            [ObjActividad setTxtdireccion:[[arrayJson objectAtIndex:i] objectForKey:@"txtdireccion"]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtfechaevento"] == nil)
                [ObjActividad setTxtfechaevento:@""];
            else
                [ObjActividad setTxtfechaevento:[[arrayJson objectAtIndex:i] objectForKey:@"txtfechaevento"]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"] == nil)
                [ObjActividad setTxtimagen:@"sgt_lugar/Nula.jpg"];
            else
                [ObjActividad setTxtimagen:[[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]];
            
            [ObjActividad setTxtnombre:[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"]];
            [ObjActividad setEsfavorito:[[[arrayJson objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
            
            
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"estacionamientobicicleta"] == nil)
                [ObjActividad setEstacionamientobicicleta:0];
            else
                [ObjActividad setEstacionamientobicicleta:[[[arrayJson objectAtIndex:i] objectForKey:@"estacionamientobicicleta"] integerValue]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"estacionamientodiscapacitado"] == nil)
                [ObjActividad setEstacionamientodiscapacitado:0];
            else
                [ObjActividad setEstacionamientodiscapacitado:[[[arrayJson objectAtIndex:i] objectForKey:@"estacionamientodiscapacitado"] integerValue]];
            
            
            NSString *queryInsert = [NSString stringWithFormat:@"INSERT INTO ACTIVIDADES(codactividad,institucion,numdia,numesactividad,nummes,txtdireccion,txtfechaevento,txtimagen,txtnombre,esfavorito,estacionamientobicicleta,estacionamientodiscapacitado) VALUES ('%ld','%@','%ld','%ld','%ld','%@','%@','%@','%@','%ld','%ld','%ld')",(long)ObjActividad.codactividad,ObjActividad.institucion,(long)ObjActividad.numdia,(long)ObjActividad.numesactividad,(long)ObjActividad.nummes,ObjActividad.txtdireccion,ObjActividad.txtfechaevento,ObjActividad.txtimagen,ObjActividad.txtnombre,(long)ObjActividad.esfavorito,(long)ObjActividad.estacionamientobicicleta,(long)ObjActividad.estacionamientodiscapacitado];
            
            [[SyncronizeData sharedInstance]executeQuery:queryInsert];
            ObjActividad = nil;
            
        }
    }
}

@end
