//
//  LugarCell.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "LugarCell.h"

@implementation LugarCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)llenarCelda:(Lugar *)lugar{
    //imgIcono.image = [UIImage imageNamed:lugar.imagen];
    lblNombre.text = lugar.txtnombre;
    lblDireccion.text = lugar.txtdireccion;
    
    NSString *img = lugar.txtimagen;
    
    [imgBanner setBackgroundColor:[UIColor lightGrayColor]];
    
    img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
    
    if ([self verificarRutaImagenLocal:img]) {
        [imgBanner setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
    }else{
        [imgBanner setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,lugar.txtimagen]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LA WEB"];
    }
    
}

- (BOOL)verificarRutaImagenLocal:(NSString *)nombreImagen{
    NSString *tempR = [NSString stringWithFormat:@"%@/%@",[self imagePath],nombreImagen];
    BOOL result =[[NSFileManager defaultManager] fileExistsAtPath:tempR];
    return result;
}

- (NSString *)imagePath{
    NSArray *rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directorioDocumentos = [rutas objectAtIndex:0];
    NSString *rutaimages=[NSString stringWithFormat:@"%@/%@",directorioDocumentos,nombreCarpeta];
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:rutaimages]){
        [fileManagerM createDirectoryAtPath:rutaimages withIntermediateDirectories:YES attributes:nil error:nil];
        //[Soporte dump:@"creando la carpeta"];
    }else{
        //[Soporte dump:@"la carpeta ya fue creada"];
    }
    
    return rutaimages;
}

@end
