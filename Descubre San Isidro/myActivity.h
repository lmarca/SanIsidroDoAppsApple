//
//  myActivity.h
//  Agenda
//
//  Created by Procesos on 8/05/15.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"
#import "Soporte.h"
#import "NSUserDefaults+Convenience.h"
#import "NSString+Utiles.h"
#import "Utiles.h"
#import "Constantes.h"
#import "ConnectWS.h"
#import "SyncronizeData.h"
#import "Memoria.h"
#import "FechaHora.h"
#import "Util_Color.h"
#import "AsyncImageView.h"

@interface myActivity : UIViewController
{
    LoadingView *vLoading;
    
@protected
    NSString *navigatorTitleText;//keely
    
@private
    UIView *viewFondoNavegador;
    UILabel *lbTiempoInactividad;
}


@property (nonatomic, strong) id centerViewController;

- (void)addItems;
- (void)addLogOut;
- (void)Back;

- (void)getTitle:(NSString *)msj;

- (void)show:(NSString *)Class;
- (void)showCancel;
- (void)showBack;
- (void) AlertDismiss:(NSString *)message;
- (void) SetAlerta:(NSString *)title message:(NSString *)message;

- (void)LoadingOn:(NSString *)msg;
- (void)LoadingOff;
- (void)setLeftPadding:(int) paddingValue textField:(UITextField *)textField;
- (void)setImageToTextField:(UITextField *)textField nameImg:(NSString *)nameImg;
- (void)circleImage:(UIImageView *)image;
- (void)callToPhone:(NSString *)phone;
- (BOOL)verificarRutaImagenLocal:(NSString *)nombreImagen;
- (NSString *)imagePath;
- (void)DeleteImagePath:(NSString *)ruta;

@end
