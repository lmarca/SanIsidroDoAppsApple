//
//  CellMenuLug.m
//  SanIsidroApp
//
//  Created by Luis on 8/05/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "CellMenuLug.h"

@implementation CellMenuLug

@synthesize nombre, thumb, button, content;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)llenarCelda:(Menuportadalugar *)menu{
    
    content.layer.cornerRadius = 3;
    content.layer.masksToBounds = YES;
    
    nombre.text = menu.txtnombre;
    
    NSString *img = menu.txticono;
    
    //[thumb setBackgroundColor:[UIColor lightGrayColor]];
    
    img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
    
    if ([self verificarRutaImagenLocal:img]) {
        [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
    }else{
        [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,menu.txticono]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LA WEB"];
    }
    
}

- (BOOL)verificarRutaImagenLocal:(NSString *)nombreImagen{
    NSString *tempR = [NSString stringWithFormat:@"%@/%@",[self imagePath],nombreImagen];
    BOOL result =[[NSFileManager defaultManager] fileExistsAtPath:tempR];
    return result;
}

- (NSString *)imagePath{
    NSArray *rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directorioDocumentos = [rutas objectAtIndex:0];
    NSString *rutaimages=[NSString stringWithFormat:@"%@/%@",directorioDocumentos,nombreCarpeta];
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:rutaimages]){
        [fileManagerM createDirectoryAtPath:rutaimages withIntermediateDirectories:YES attributes:nil error:nil];
        //[Soporte dump:@"creando la carpeta"];
    }else{
        //[Soporte dump:@"la carpeta ya fue creada"];
    }
    
    return rutaimages;
}

@end
