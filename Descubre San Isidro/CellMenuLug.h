//
//  CellMenuLug.h
//  SanIsidroApp
//
//  Created by Luis on 8/05/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Menuportadalugar.h"
#import "AsyncImageView.h"

@interface CellMenuLug : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet AsyncImageView *thumb;
@property (strong, nonatomic) IBOutlet UILabel *nombre;
@property (strong, nonatomic) IBOutlet UIView *content;


-(void)llenarCelda:(Menuportadalugar *)menu;

@end
