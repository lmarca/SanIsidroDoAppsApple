//
//  Utiles.h
//  Agenda
//
//  Created by Procesos on 8/05/15.
//  Copyright (c) 2015 com.procesosmc.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

@interface Utiles : NSObject

+(BOOL)isInternetAvailable;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (NSString *)retornaFechaFormat:(NSString *)fecha;

@end
