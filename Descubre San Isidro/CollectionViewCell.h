//
//  CollectionViewCell.h
//  Descubre San Isidro
//
//  Created by Luis on 28/06/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "MenuPortadaactividad.h"

@interface CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *fondo;
@property (strong, nonatomic) IBOutlet AsyncImageView *thumb;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIButton *button;


-(void)llenarCelda:(MenuPortadaactividad *)menu;


@end
