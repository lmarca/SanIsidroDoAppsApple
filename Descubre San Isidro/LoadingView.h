//
//  SyncronizeData.h
//  SanIsidroApp
//
//  Created by DoApps on 26/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView{
    UIView *viewLoading;
    UIActivityIndicatorView *spinner;
    UILabel *lblLoading;
}

-(void)loadViewsWithMessage:(NSString *)message;
-(void)startLoading;
-(void)stopLoading;
- (BOOL)isLoading;

@end
