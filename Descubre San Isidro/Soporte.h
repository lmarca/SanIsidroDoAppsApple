//
//  Soporte.h
//  Demo
//
//  Created by Procesos on 2/02/15.
//  Copyright (c) 2015 com.procesosmc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Soporte : NSObject


+ (int )toInt:(NSString *)valor;
+ (short )toShort:(NSString *)valor;
+ (double )toDouble:(NSString *)valor;
+ (void)dump:(NSString *)prefijo exception:(NSException *)e;
+ (void)dump:(NSString *)prefijo E:(NSString *)e;
+ (void)dump:(NSString *)message prefijo:(NSObject *)e;
+ (void)dump:(NSString *)mensaje;
+ (void)alert:(NSString *)mensaje;
+ (void)err:(NSException *)mensaje;
+ (BOOL)isNullOrE:(NSString *)texto;
+ (BOOL)isNullOrEmpty:(NSString *)texto;
+ (BOOL) validEmail:(NSString*)emailString;
+ (BOOL )listMatches: (NSString *)pattern stringT:(NSString *)string;
+ (BOOL)validaTelefono:(NSString *)numero;
+ (BOOL)isGmail:(NSString *)email;
+ (BOOL)isYahoo:(NSString *)email;
+ (BOOL) validEmail:(NSString*)emailString regExPattern:(NSString *)regExPattern;
+ (NSArray*)regexToArray:(NSString*) regexPattern string:(NSString *)string;

@end
