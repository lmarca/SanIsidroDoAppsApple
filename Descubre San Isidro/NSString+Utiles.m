//
//  NSString+Utiles.m
//  MPOS
//
//  Created by Procesos on 19/08/15.
//  Copyright (c) 2015 com.procesosmc. All rights reserved.
//

#import "NSString+Utiles.h"

@implementation NSString (Utiles)

- (BOOL)isBlank{
    if([[self stringByStrippingWhitespace] isEqualTo:@""])
        return YES;
    return NO;
}

- (BOOL)isEqualTo:(NSString *)otherString{
    if ([self caseInsensitiveCompare:otherString] == NSOrderedSame)
        return YES;
    return NO;
}

- (BOOL)isPrefix:(NSString *)otherString{
    return [self hasPrefix:otherString];
}

- (BOOL)isSuffix:(NSString *)otherString{
    return [self hasSuffix:otherString];
}

- (BOOL)contains:(NSString*)string {
    return [self rangeOfString:string].location != NSNotFound;
}

- (BOOL)containsOnlyLetters{
    NSCharacterSet *blockedCharacters = [[NSCharacterSet letterCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
}

- (BOOL)containsOnlyNumbers{
    NSCharacterSet *numbers = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    return ([self rangeOfCharacterFromSet:numbers].location == NSNotFound);
}

- (BOOL)containsOnlyNumbersAndLetters{
    NSCharacterSet *blockedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
}

- (NSArray *)splitOnChar:(char)ch {
    NSMutableArray *results = [[NSMutableArray alloc] init];
    int start = 0;
    for(int i=0; i<[self length]; i++) {
        
        BOOL isAtSplitChar = [self characterAtIndex:i] == ch;
        BOOL isAtEnd = i == [self length] - 1;
        
        if(isAtSplitChar || isAtEnd) {
            //take the substring &amp; add it to the array
            NSRange range;
            range.location = start;
            range.length = i - start + 1;
            
            if(isAtSplitChar)
                range.length -= 1;
            
            [results addObject:[self substringWithRange:range]];
            start = i + 1;
        }
        
        //handle the case where the last character was the split char.  we need an empty trailing element in the array.
        if(isAtEnd && isAtSplitChar)
            [results addObject:@""];
    }
    
    return results;
}

- (NSString *)stringByStrippingWhitespace {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)reemplazarCaracteresCadena:(NSString *)caracterAntiguo
                        conNuevoCaracter:(NSString *)caracterNuevo{
    return [self stringByReplacingOccurrencesOfString:caracterAntiguo withString:caracterNuevo];
}

- (NSString*)addString:(NSString*)string{
    if(!string || string.length == 0)
        return self;
    return [self stringByAppendingString:string];
}

+ (NSString *)PadtoLeft:(int)value forDigits:(int)zeros{
    NSString *format = [NSString stringWithFormat:@"%%0%dd", zeros];
    return [NSString stringWithFormat:format,value];
}


@end
