//
//  SyncronizeData.h
//  SanIsidroApp
//
//  Created by DoApps on 26/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

-(void)loadViewsWithMessage:(NSString *)message{
    float padding = 10.0;
    CGSize size = [UIScreen mainScreen].bounds.size;
    self.frame = CGRectMake(0, 0, size.width, size.height);
    self.backgroundColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.4];
    viewLoading = [[UILabel alloc] init];
    viewLoading.frame = CGRectMake(0, 0, size.width/2, 100);
    viewLoading.backgroundColor = [UIColor darkTextColor];
    //viewLoading.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:25.0/255.0 blue:40.0/255.0 alpha:01];
    viewLoading.center = CGPointMake(size.width/2, size.height/2);
    viewLoading.layer.cornerRadius = 10;
    viewLoading.layer.masksToBounds = YES;
    [self addSubview:viewLoading];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(CGRectGetWidth(viewLoading.frame)/2, CGRectGetHeight(viewLoading.frame)/2-padding);
    [viewLoading addSubview:spinner];
    
    if (message.length != 0) {
        lblLoading = [[UILabel alloc] init];
        lblLoading.text = message;
        lblLoading.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
        [lblLoading sizeToFit];
        lblLoading.center = CGPointMake(spinner.center.x, spinner.frame.origin.y+spinner.frame.size.height+padding);
        lblLoading.textColor = [UIColor whiteColor];
        [viewLoading addSubview:lblLoading];
    }
}

-(BOOL)isLoading{
    if ([spinner isAnimating]) {
        return true;
    }else{
        return false;
    }
}

-(void)startLoading{
    [spinner startAnimating];
}

-(void)stopLoading{
    [spinner stopAnimating];
    [self removeFromSuperview];
}


@end
