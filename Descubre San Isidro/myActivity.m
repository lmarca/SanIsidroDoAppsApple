//
//  myActivity.m
//  Agenda
//
//  Created by Procesos on 8/05/15.
//  Copyright © 2016 peru. All rights reserved.
//

#import "myActivity.h"
#import "AppDelegate.h"

@interface myActivity ()
{
    NSTimer                  *myTimer;
    UIAlertView              *myAlert;
    int a,b;
    UIButton                 *btnLogOut,*btnBack;
}

@end

@implementation myActivity

static AppDelegate *delegadoApp;

- (void)viewDidLoad{
    
    [self.navigationItem setHidesBackButton:true];
    // Do any additional setup after loading the view.
}


- (void)getTitle:(NSString *)msj{
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont boldSystemFontOfSize:20], NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = msj;
}


//- (UIStatusBarStyle)preferredStatusBarStyle {
//    if (self.centerViewController) {
//        if ([self.centerViewController isKindOfClass:[UINavigationController class]]) {
//            return [((UINavigationController *)self.centerViewController).topViewController preferredStatusBarStyle];
//        }
//        return [self.centerViewController preferredStatusBarStyle];
//    }
//    return UIStatusBarStyleDefault;
//}

//- (BOOL)prefersStatusBarHidden{
//    return NO;
//}


- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addItems{
    UIButton *homeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setImage:[UIImage imageNamed:@"menu-calendario"] forState:UIControlStateNormal];
    //[homeBtn addTarget:self action:@selector(calendario) forControlEvents:UIControlEventTouchUpInside];
    homeBtn.adjustsImageWhenDisabled = false;
    homeBtn.adjustsImageWhenHighlighted = false;
    [homeBtn setFrame:CGRectMake(0, 0, 22, 22)];
    
    UIButton *settingsBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsBtn setImage:[UIImage imageNamed:@"plus79"] forState:UIControlStateNormal];
    //[settingsBtn addTarget:self action:@selector(addCurso) forControlEvents:UIControlEventTouchUpInside];
    settingsBtn.adjustsImageWhenDisabled = false;
    settingsBtn.adjustsImageWhenHighlighted = false;
    [settingsBtn setFrame:CGRectMake(44, 0, 22, 22)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 22)];
    [rightBarButtonItems addSubview:homeBtn];
    [rightBarButtonItems addSubview:settingsBtn];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
}

- (void)addLogOut{
    btnLogOut = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogOut setImage:[UIImage imageNamed:@"ic_menu"] forState:UIControlStateNormal];
    //[btnLogOut addTarget:self action:@selector(habilitarMenu) forControlEvents:UIControlEventTouchUpInside];
    btnLogOut.frame = CGRectMake(0.0f, 0.0f, 34.0f, 32.0f);
    UIBarButtonItem *btnCarritoItem = [[UIBarButtonItem alloc] initWithCustomView:btnLogOut];
    btnLogOut.adjustsImageWhenHighlighted = false;
    btnLogOut.adjustsImageWhenDisabled = false;
    [self.navigationItem setLeftBarButtonItem:btnCarritoItem];
}


- (void)Back{
    btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackTo) forControlEvents:UIControlEventTouchUpInside];
    btnBack.frame = CGRectMake(0.0f, 0.0f, 34.0f, 32.0f);
    UIBarButtonItem *btnCarritoItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    [self.navigationItem setLeftBarButtonItem:btnCarritoItem];
}

- (void)BackTo{
    [self.navigationController popViewControllerAnimated:YES];
}


/*
 Permite navegar durante la actividad en la App.
 @param "" : Contiene el nombre de la clase a hacer el push.
 */
- (void)showCancel{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)show:(NSString *)Class{
    dispatch_async(dispatch_get_main_queue(),^(void){
        UIStoryboard *storyboard = self.storyboard;
        UIViewController *detail = [storyboard instantiateViewControllerWithIdentifier:Class];
        [self.navigationController pushViewController:detail animated: YES];
    });
}

- (void)showBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)LoadingOn:(NSString *)msg{
    delegadoApp = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegadoApp LoadingOn:msg];
}

- (void)LoadingOff{
    [delegadoApp LoadingOff];
}

- (void) setLeftPadding:(int) paddingValue textField:(UITextField *)textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, textField.frame.size.height)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)setImageToTextField:(UITextField *)textField nameImg:(NSString *)nameImg{
    [[textField valueForKey:@"textInputTraits"]
     setValue:[UIColor clearColor]
     forKey:@"insertionPointColor"];
    
    UIImage* img = [UIImage imageNamed:nameImg];   // non-CocoaPods
    if (img != nil) textField.rightView = [[UIImageView alloc] initWithImage:img];
    textField.rightView.contentMode = UIViewContentModeScaleAspectFit;
    textField.rightViewMode = UITextFieldViewModeAlways;
    textField.rightView.clipsToBounds = YES;
}

/*
 Permite mostrar un mensaje en una vista en un UIAlertView(Toast)
 @param message: Contiene el titulo que se mostrara en el UIAlertView
 */

- (void)AlertDismiss:(NSString *)message{
    myAlert = [[UIAlertView alloc]initWithTitle:nil message:message delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    myTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(cancelAlert) userInfo:nil repeats:NO];
    [myAlert show];
}

- (void)cancelAlert{
    [myAlert dismissWithClickedButtonIndex:-1 animated:YES];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [myTimer invalidate];
}


/*
 Permite mostrar un mensaje en una vista en un UIAlertView(Popup)
 @param message: Contiene el titulo que se mostrara en el UIAlertView
 */
- (void) SetAlerta:(NSString *)title message:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"Aceptar"
                                         otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}

- (void)circleImage:(UIImageView *)image{
    image.contentMode = UIViewContentModeScaleAspectFill;
    image.layer.cornerRadius = image.frame.size.height/2;
    image.layer.masksToBounds = YES;
    image.layer.borderColor = [UIColor lightGrayColor].CGColor;
    image.layer.borderWidth = 0.5;
}

- (void)callToPhone:(NSString *)phone{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (BOOL)verificarRutaImagenLocal:(NSString *)nombreImagen{
    NSString *tempR = [NSString stringWithFormat:@"%@/%@",[self imagePath],nombreImagen];
    [Soporte dump:@"rutaLocal" E:tempR];
    BOOL result =[[NSFileManager defaultManager] fileExistsAtPath:tempR];
    return result;
}

- (NSString *)imagePath{
    NSArray *rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directorioDocumentos = [rutas objectAtIndex:0];
    NSString *rutaimages=[NSString stringWithFormat:@"%@/%@",directorioDocumentos,nombreCarpeta];
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:rutaimages]){
        [fileManagerM createDirectoryAtPath:rutaimages withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return rutaimages;
}

- (void)DeleteImagePath:(NSString *)ruta{
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:ruta]){
        [fileManagerM removeItemAtPath:ruta error:nil];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
