//
//  ActividadCell.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "ActividadCell.h"

@implementation ActividadCell

static inline NSString *stringFromWeekday(NSInteger weekday){
    static NSString *strings[] = {
        @"ENE",
        @"FEB",
        @"MAR",
        @"ABR",
        @"MAY",
        @"JUN",
        @"JUL",
        @"AGO",
        @"SET",
        @"OCT",
        @"NOV",
        @"DIC",
    };
    
    return strings[weekday -1];
}

- (void)awakeFromNib {
    lblInstitucion.layer.cornerRadius = 5;
    lblInstitucion.layer.masksToBounds = YES;
    
    imgBanner.layer.masksToBounds = YES;
    //[UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0]
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)llenarCelda:(Actividad *)actividad isSucediendo:(BOOL)isSucediendo{
    
    //lblInstitucion.text = [actividad.institucion uppercaseString];
    
    UILabel *label = [[UILabel alloc]init];
    //adjust the label the the new height.
    label.text = [actividad.institucion uppercaseString];
    label.font = [UIFont fontWithName:@"Helvetica" size:9.0];
    
    CGSize textSize = [[label text] sizeWithAttributes:@{NSFontAttributeName:[label font]}];
    
    CGFloat strikeWidth = textSize.width;
    
    label.frame = CGRectMake(20, 141, strikeWidth + 10, 15);
    label.textColor = [UIColor colorWithHexString:@"4d4d4d" andAlpha:1.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.cornerRadius = 5;
    label.layer.masksToBounds = YES;
    label.backgroundColor = [UIColor colorWithHexString:@"e3e3e3" andAlpha:1.0];
    [self.contentView addSubview:label];
    
    lblNombre.text = actividad.txtnombre;
    lblDescripcion.text = isSucediendo?f(@"Iniciará en %@",actividad.tiempodeiniciado):actividad.txtdireccion;
    
    [imgBanner setBackgroundColor:[UIColor lightGrayColor]];
    NSString *img = actividad.txtimagen;
    
    img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
    
    if ([self verificarRutaImagenLocal:img]) {
        [imgBanner setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
    }else{
        [imgBanner setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,actividad.txtimagen]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LA WEB"];
    }
    
    if (!isSucediendo) {
        if (actividad.numdia > 0) {
            lblDia.text = f(@"%ld",actividad.numdia);
            lblDia.textColor = [UIColor colorWithHexString:@"9E0053" andAlpha:1.0];
            //content.layer.borderColor = [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0].CGColor;
            //content.layer.borderWidth = 2.0f;
            content.layer.backgroundColor = [UIColor colorWithHexString:@"e3e3e3" andAlpha:1.0].CGColor;
        }
        
        if (actividad.txtfechaevento.length > 0){
            lblMes.text = stringFromWeekday([[[actividad.txtfechaevento substringFromIndex:5]substringToIndex:2]integerValue]);
            lblMes.textColor = [UIColor colorWithHexString:@"4d4d4d" andAlpha:1.0];
        }
        
    }
}

- (BOOL)verificarRutaImagenLocal:(NSString *)nombreImagen{
    NSString *tempR = [NSString stringWithFormat:@"%@/%@",[self imagePath],nombreImagen];
    BOOL result =[[NSFileManager defaultManager] fileExistsAtPath:tempR];
    return result;
}

- (NSString *)imagePath{
    NSArray *rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directorioDocumentos = [rutas objectAtIndex:0];
    NSString *rutaimages=[NSString stringWithFormat:@"%@/%@",directorioDocumentos,nombreCarpeta];
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:rutaimages]){
        [fileManagerM createDirectoryAtPath:rutaimages withIntermediateDirectories:YES attributes:nil error:nil];
        //[Soporte dump:@"creando la carpeta"];
    }else{
        //[Soporte dump:@"la carpeta ya fue creada"];
    }
    
    return rutaimages;
}

@end
