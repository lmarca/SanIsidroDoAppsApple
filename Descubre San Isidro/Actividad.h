//
//  Actividad.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncronizeData.h"

@interface Actividad : NSObject

@property(nonatomic) NSInteger codactividad;
@property(nonatomic,strong) NSString *institucion;
@property(nonatomic) NSInteger numdia;
@property(nonatomic) NSInteger numesactividad;
@property(nonatomic) NSInteger nummes;
@property(nonatomic,strong) NSString *txtdescripcion;
@property(nonatomic,strong) NSString *txtdireccion;
@property(nonatomic,strong) NSString *txtfechaevento;
@property(nonatomic,strong) NSString *txtimagen;
@property(nonatomic,strong) NSString *txtnombre;
@property(nonatomic) float numx;
@property(nonatomic) float numy;
@property(nonatomic,strong) NSString *txthorariosatencion;
@property(nonatomic,strong) NSString *logo;
@property(nonatomic,strong) NSString *texto;
@property(nonatomic,strong) NSString *tiempodeiniciado;
@property(nonatomic) NSInteger esfavorito;
@property(nonatomic) NSInteger estacionamientobicicleta;
@property(nonatomic) NSInteger estacionamientodiscapacitado;


+ (void)InsertActividades:(NSArray *)arrayJson;

@end
