//
//  Lugar.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "Lugar.h"

@implementation Lugar

+ (void)InsertLugares:(NSArray *)arrayJson{
    
    if (arrayJson.count != 0){
        //GUARDAMOS EN LA BD
        for (int i = 0; i<arrayJson.count; i++) {
            
            Lugar *ObjLugar = [Lugar new];
            [ObjLugar setCodlugar:[[[arrayJson objectAtIndex:i] objectForKey:@"codlugar"] integerValue]];
            [ObjLugar setNumx:[[[arrayJson objectAtIndex:i] objectForKey:@"numx"] floatValue]];
            [ObjLugar setNumy:[[[arrayJson objectAtIndex:i] objectForKey:@"numy"] floatValue]];
            [ObjLugar setTxtdireccion:[[arrayJson objectAtIndex:i] objectForKey:@"txtdireccion"]];
            [ObjLugar setTxthorariosatencion:[[arrayJson objectAtIndex:i] objectForKey:@"txthorariosatencion"]];
            [ObjLugar setTxtnombre:[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"] == nil)
                [ObjLugar setTxtimagen:@"sgt_lugar/Nula.jpg"];
            else
                [ObjLugar setTxtimagen:[[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"] == nil)
                [ObjLugar setTxtdescripcion:@""];
            else
                [ObjLugar setTxtdescripcion:[[arrayJson objectAtIndex:i] objectForKey:@"txtdescripcion"]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txttelefono"] == nil)
                [ObjLugar setTxttelefono:@""];
            else
                [ObjLugar setTxttelefono:[[arrayJson objectAtIndex:i] objectForKey:@"txttelefono"]];
            
            if ([[arrayJson objectAtIndex:i] objectForKey:@"txtpaginaweb"] == nil)
                [ObjLugar setTxtpaginaweb:@""];
            else
                [ObjLugar setTxtpaginaweb:[[arrayJson objectAtIndex:i] objectForKey:@"txtpaginaweb"]];
            
            [ObjLugar setEsfavorito:[[[arrayJson objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
            
            NSString *queryInsert = [NSString stringWithFormat:@"INSERT INTO LUGARES(codlugar,numx,numy,txtdescripcion,txtdireccion,txthorariosatencion,txtimagen,txtnombre,txttelefono,txtpaginaweb,esfavorito) VALUES ('%ld','%f','%f','%@','%@','%@','%@','%@','%@','%@','%ld')",(long)ObjLugar.codlugar,ObjLugar.numx,ObjLugar.numy,ObjLugar.txtdescripcion,ObjLugar.txtdireccion,ObjLugar.txthorariosatencion,ObjLugar.txtimagen,ObjLugar.txtnombre,ObjLugar.txtnombre,ObjLugar.txtpaginaweb,(long)ObjLugar.esfavorito];
            [[SyncronizeData sharedInstance]executeQuery:queryInsert];
            ObjLugar = nil;
            
        }
    }
}

@end
