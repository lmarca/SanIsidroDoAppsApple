//
//  RegisterVC.m
//  SanIsidroApp
//
//  Created by Luis on 3/05/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "RegisterVC.h"
#import "AppDelegate.h"

@interface RegisterVC (){
    
    IBOutlet UITextField *txtNombre;
    IBOutlet UITextField *txtCorreo;
    IBOutlet UILabel *lblVersion;
    
    
}

@property(readwrite)int  lastTag;

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLeftPadding:05 textField:txtNombre];
    [self setLeftPadding:05 textField:txtCorreo];
    
    txtNombre.layer.borderColor=[[UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0] CGColor];
    txtNombre.layer.borderWidth=1.0;
    
    txtCorreo.layer.borderColor=[[UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0] CGColor];
    txtCorreo.layer.borderWidth=1.0;
    
    _lastTag = 1;
    
    lblVersion.text = @"Versión 1.2\nMunicipalidad de San Isidro\nLima - Perú";
    // Do any additional setup after loading the view.
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField.tag == _lastTag) textField.returnKeyType = UIReturnKeyDone;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger tag = textField.tag;
    if(textField.tag == _lastTag)
    {
        [textField resignFirstResponder];
        return YES;
        
    } else {
        UITextField *target = (UITextField *)[self.view viewWithTag:tag+1];
        [target becomeFirstResponder];
        return NO;
    }
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self animatedTextField:textField up:NO];
    [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self animatedTextField:textField up:YES];
}

- (void)animatedTextField:(UITextField *)textField up:(BOOL)up{
    int animatedDistance;
    int moveUpValue;
    if (IS_IPHONE4)
        moveUpValue =textField.frame.origin.y + textField.frame.size.height+50;
    else if (IS_IPHONE5)
        moveUpValue =textField.frame.origin.y + textField.frame.size.height-10;
    else if (IS_IPHONE6)
        moveUpValue =textField.frame.origin.y + textField.frame.size.height-105;
    else
        moveUpValue =textField.frame.origin.y + textField.frame.size.height-160;
    
    animatedDistance=216-(500-moveUpValue-5);
    if (animatedDistance>0) {
        const int movementDistance =animatedDistance;
        const float movementDuration=0.3;
        int movement=(up ? -movementDistance:movementDistance);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:movementDuration];
        self.view.frame=CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onCrearCuenta:(id)sender {
    if ([Utiles isInternetAvailable]) {
        
        if(![Soporte validEmail:txtCorreo.text regExPattern:@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$"]) {
            [self AlertDismiss:NSLocalizedString(@"APP_VALIDATE_MAIL", nil)];
            return;
        }
        if (txtNombre.hasText && txtCorreo.hasText) {
            if(txtNombre.isFirstResponder) [txtNombre resignFirstResponder];
            if(txtCorreo.isFirstResponder) [txtCorreo resignFirstResponder];
            
            [self LoadingOn:NSLocalizedString(@"APP_REGISTER", nil)];
            
            NSString *post = [NSString stringWithFormat:@"email=%@&name=%@&porRedSocial=false",txtCorreo.text,txtNombre.text];
            
            [ConnectWS findAllObjectsWithQuery:f(@"%@security/%@",urlWS,@",/grabarusuario") query:post state:1 withBlock:^(id userInfo, NSError *error) {
                
                [self LoadingOff];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                               {
                                   if (userInfo) {
                                       [Soporte dump:@"" prefijo:userInfo];
                                       
                                       NSDictionary *dictJson = (NSDictionary *)userInfo;
                                       
                                       if([dictJson[@"success"] boolValue]){
                                           [NSStandardUserDefaults saveObject:dictJson[@"ba_user_id"] forKey:ba_user_id];
                                           [NSStandardUserDefaults saveObject:dictJson[@"email"] forKey:email];
                                           [NSStandardUserDefaults saveObject:dictJson[@"name"] forKey:name];
                                           [NSStandardUserDefaults saveObject:dictJson[@"login_user"] forKey:login_user];
                                           [NSStandardUserDefaults saveBool:true forKey:Sesion];
                                           [NSStandardUserDefaults saveBool:false forKey:isRedesSociales];
                                       }
                                   }
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if ([NSStandardUserDefaults boolForKey:Sesion]) {
                                           AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                           [appDelegate loadTabBar];
                                       }
                                   });
                               });

                
            }];
        }
        else [self SetAlerta:nil message:NSLocalizedString(@"APP_FIELD_ENTER", nil)];
    }
    else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
}

- (IBAction)backTo:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)onSaltarApp:(id)sender {
}


@end
