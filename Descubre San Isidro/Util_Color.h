//
//  Util_Colores.h
//  Life
//
//  Created by User on 2/12/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor(ColorUtils)

+(UIColor *)colorWithHex:(UInt32)col andAlpha:(CGFloat)aplha;
+(UIColor *)colorWithHexString:(NSString *)str andAlpha:(CGFloat)aplha;

@end
