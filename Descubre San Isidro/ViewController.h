//
//  ViewController.h
//  SanIsidroApp
//
//  Created by Luis on 26/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginPrincipalVC.h"
#import "ActividadCell.h"
#import "LugarCell.h"
#import "DetalleActividadVC.h"
#import "DetalleLugarVC.h"
#import "myActivity.h"
#import "Actividad.h"
#import "Lugar.h"

@interface ViewController : myActivity <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) Lugar *lugar;
@property (nonatomic) Actividad *actividad;

@end

