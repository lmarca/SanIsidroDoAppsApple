//
//  EventoCell.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "EventoCell.h"

@implementation EventoCell

- (void)awakeFromNib {
    // Initialization code
    imgBanner.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)llenarCelda:(Evento *)evento{
    lblCreador.text = evento.creador;
    lblDireccion.text = evento.direccion;
    imgBanner.image = [UIImage imageNamed:evento.foto];
}

@end
