//
//  DetalleLugarVC.m
//  SanIsidroApp
//
//  Created by Luis on 1/05/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "DetalleLugarVC.h"

@interface DetalleLugarVC (){
    NSMutableString *horarioxlugar;
    BOOL isHorarios;
}

@end

@implementation DetalleLugarVC

@synthesize tblDetalleLugar, lugar, listaHorariosxlugar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    horarioxlugar = [NSMutableString string];
    
    [self setAutomaticallyAdjustsScrollViewInsets:true];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    for (int i = 0; i<listaHorariosxlugar.count; i++) {
        
        if ([[listaHorariosxlugar objectAtIndex:i] objectForKey:@"etiqueta"] != NULL) {
            [horarioxlugar appendString:[[listaHorariosxlugar objectAtIndex:i] objectForKey:@"etiqueta"]];
            [horarioxlugar appendString:@"\n"];
        }
        if ([[listaHorariosxlugar objectAtIndex:i] objectForKey:@"texto"] != NULL) {
            [horarioxlugar appendString:[[listaHorariosxlugar objectAtIndex:i] objectForKey:@"texto"]];
            [horarioxlugar appendString:@"\n\n"];
        }

    }
    
    if (listaHorariosxlugar.count > 0)
        isHorarios = TRUE;
    else
        isHorarios = FALSE;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)btnBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShared:(id)sender {
    [self actionShared];
}

- (void)actionShared{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Compartir" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",
                            @"Twitter",
                            @"WhatsApp",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self FBShareFacebook];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self sharedWhatsApp];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void)sharedWhatsApp {
    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
        
        NSString *Picture = nil;
        
        if (![lugar.txtimagen isBlank]){
            Picture = lugar.txtimagen;
            Picture = [Picture reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        }
        
        UIImage     *iconImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],Picture]];
        NSString    *savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
        
        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
        
        _documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        _documentInteractionController.UTI = @"net.whatsapp.image";
        _documentInteractionController.delegate = self;
        
        [_documentInteractionController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
        
    } else {
        [self SetAlerta:@"Alerta" message:NSLocalizedString(@"APP_INSTALL_WSP", nil)];
    }
}

- (void)FBShareFacebook{
    if(![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])  {
        [Soporte dump:@"log output of your choice here"];
    }
    // Facebook may not be available but the SLComposeViewController will handle the error for us.
    mySLComposerSheet = [[SLComposeViewController alloc] init];
    mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [mySLComposerSheet setInitialText:lugar.txtdescripcion];
    
    if (![lugar.txtimagen isBlank]){
        NSString *MICONO = lugar.txtimagen;
        MICONO = [MICONO reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        [mySLComposerSheet addImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],MICONO]]]; //an image you could post
    }
    
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        }
        if (![output isEqualToString:@"Action Cancelled"]) {
            // Only alert if the post was a success. Or not! Up to you.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)TwitterShare{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *twitter =
        [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [twitter setInitialText:lugar.txtdescripcion];
        [twitter addURL:[NSURL URLWithString:@"http://www.google.com"]];
        
        if (![lugar.txtimagen isBlank]){
            NSString *MICONO = lugar.txtimagen;
            MICONO = [MICONO reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
            [twitter addImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],MICONO]]]; //an image you could post
        }
        
        [self presentViewController:twitter animated:YES completion:nil];
    } else {
        [Soporte dump:@"it is not configured"];
    }
}

- (IBAction)btnFavorite:(id)sender {
    
    NSInteger idUser = [[Memoria sharedInstance]obtenerUserId];
    if (idUser == 0 || idUser == -1) {
        [self AlertDismiss:NSLocalizedString(@"APP_LOGIN_ABOUT", nil)];
        return;
    }
    
    if ([Utiles isInternetAvailable]) {
        
        NSString *pathURL = nil;
        
        if (lugar.esfavorito == 1) {
            pathURL = @"eliminarfavorito";
        }else{
            pathURL = @"grabarfavorito";
        }
        
        NSString *post = [NSString stringWithFormat:@"userId=%ld&numlugar=%ld",idUser,lugar.codlugar];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@security/%@/%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",pathURL) query:post state:1 withBlock:^(id userInfo, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    if ([dictJson[@"success"]boolValue]) {
                        [self AlertDismiss:lugar.esfavorito == 1 ? NSLocalizedString(@"APP_DELETE_PLACE", nil): NSLocalizedString(@"APP_INSERT_PLACE", nil)];
                        if (lugar.esfavorito == 1) {
                            lugar.esfavorito = 0;
                        }else{
                            lugar.esfavorito = 1;
                        }
                        [tblDetalleLugar reloadData];
                    }
                }
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellMaster = nil;
    
    if (indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"ct01";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        AsyncImageView *thumb = (AsyncImageView *)[cell viewWithTag:40];
        UIImageView *thumbFavorite = (UIImageView *)[cell viewWithTag:60];

        [thumbFavorite setImage:[UIImage imageNamed:lugar.esfavorito == 0 ? @"ic_favorite_off.png" : @"ic_favorite_on.png"]];
        
        NSString *img = lugar.txtimagen;
        
        img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        
        if ([self verificarRutaImagenLocal:img])
            [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        else
            [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,lugar.txtimagen]];
 
        cellMaster = cell;
    }
    
    if (indexPath.row == 1) {
        
        static NSString *CellIdentifier = @"ct02";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIButton *call = (UIButton *)[cell viewWithTag:41];
        UIButton *web = (UIButton *)[cell viewWithTag:42];
        UILabel *nombre = (UILabel *)[cell viewWithTag:43];
        
        [web addTarget:self action:@selector(checkButtonWeb:) forControlEvents:UIControlEventTouchUpInside];
        [call addTarget:self action:@selector(checkButtonCall:) forControlEvents:UIControlEventTouchUpInside];
        
        call.hidden = lugar.txttelefono.length != 0 ? false : true;
        web.hidden = lugar.txtpaginaweb.length != 0 ? false : true;
        
        nombre.text = lugar.txtnombre;
        
        cellMaster = cell;
    }
    if (indexPath.row == 2) {
        
        static NSString *CellIdentifier = @"ct03";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
        return cellMaster;
    }
    if (indexPath.row == 3) {
        
        static NSString *CellIdentifier = @"ct04";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 4) {
        
        static NSString *CellIdentifier = @"ct05";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        GMSMapView *_map = (GMSMapView *)[cell viewWithTag:30];
        UILabel *_nombre = (UILabel *)[cell viewWithTag:31];
       
        // Ask for My Location data after the map has already been added to the UI.
        dispatch_async(dispatch_get_main_queue(), ^{
            _map.myLocationEnabled = YES;
        });
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lugar.numx
                                                                longitude:lugar.numy
                                                                     zoom:kZoom
                                                                  bearing:328.f
                                                             viewingAngle:40.f];
        
        _map.camera = camera;
        _map.settings.compassButton = YES;
        _map.settings.myLocationButton = YES;
        _map.delegate = self;
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(lugar.numx, lugar.numy);
        //marker.snippet = lugar.txtnombre;
        marker.icon = [UIImage imageNamed:@"location"];
        marker.draggable = NO;
        marker.map = _map;
        [_map setSelectedMarker:marker];
        _nombre.text = lugar.txtdireccion;
        
        cellMaster = cell;
    }
    
    return cellMaster;
}

- (void)checkButtonWeb:(id)sender{
    
    NSURL *url = [NSURL URLWithString:lugar.txtpaginaweb];
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Problem", nil)
                                                        message:NSLocalizedString(@"The_selected_link_cannot_be_opened", nil)
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)checkButtonCall:(id)sender{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:lugar.txttelefono];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat ktvc03 = 0;
    
    if (indexPath.row == 0) {
        ktvc03 =  200.0f;
    }
    if (indexPath.row == 1) {
        ktvc03 =  82.0f;
        
    }
    if (indexPath.row == 2) {
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"ct03";
            cell = [self.tblDetalleLugar dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
    }
    if (indexPath.row == 3) {
        
        if (isHorarios) {
            static DynamicTableViewCell *cell = nil;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                static NSString *CellIdentifier = @"ct04";
                cell = [self.tblDetalleLugar dequeueReusableCellWithIdentifier:CellIdentifier];
            });
            
            [self setUpCell:cell atIndexPath:indexPath];
            
            ktvc03 = [self calculateHeightForConfiguredSizingCell:cell];
        }else
            ktvc03 = 0.0f;

    }
    if (indexPath.row == 4) {
        ktvc03 =  327.0f;
    }
    
    return ktvc03;
}

# pragma mark - UITableViewControllerDelegate

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

# pragma mark - Cell Setup

- (void)setUpCell:(DynamicTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    [self.tblDetalleLugar setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    cell.label.text = [cell.reuseIdentifier isEqualTo:@"ct03"]? lugar.txtdescripcion : horarioxlugar;
    
    cell.label.preferredMaxLayoutWidth = self.view.frame.size.width;
}

@end
