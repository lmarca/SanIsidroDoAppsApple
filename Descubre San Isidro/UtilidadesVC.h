//
//  UtilidadesVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myActivity.h"
#import "DynamicTableViewCell.h"

@interface UtilidadesVC : myActivity <UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UITableView *tblUtilidades;


@end
