//
//  DetalleActividadVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 4/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "DetalleActividadVC.h"

@interface DetalleActividadVC (){
    IBOutlet UIImageView *imgBanner;
    IBOutlet UILabel *lblFecha;
    IBOutlet UILabel *lblNombre;
    IBOutlet UILabel *lblDescripcion;
    IBOutlet UILabel *lblDireccion;
    
    NSMutableArray *listaActividades;
    NSMutableString *strHorarioxactividad;
    NSMutableString *strEntradasxactividad;
    NSMutableString *strTextopresentacionxtipoactividad;
    BOOL isHorarios;
    BOOL isEntradas;
    CGFloat heightEstablecimiento;
    
    BOOL isOver;
}

@end

@implementation DetalleActividadVC

@synthesize tblDetalleActividad, actividad, entradasxactividad, horariosxactividad, textopresentacionxtipoactividad;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isOver = false;
    
    strHorarioxactividad = [NSMutableString string];
    strEntradasxactividad = [NSMutableString string];
    
    imgBanner.layer.masksToBounds = YES;
    
    [self setAutomaticallyAdjustsScrollViewInsets:true];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    for (int i = 0; i<horariosxactividad.count; i++) {
        
        if ([[horariosxactividad objectAtIndex:i] objectForKey:@"etiqueta"] != NULL) {
            [strHorarioxactividad appendString:[[horariosxactividad objectAtIndex:i] objectForKey:@"etiqueta"]];
            [strHorarioxactividad appendString:@"\n"];
        }
        if ([[horariosxactividad objectAtIndex:i] objectForKey:@"texto"] != NULL) {
            [strHorarioxactividad appendString:[[horariosxactividad objectAtIndex:i] objectForKey:@"texto"]];
            [strHorarioxactividad appendString:@"\n\n"];
        }
    }

    for (int i = 0; i<entradasxactividad.count; i++) {
        
        if ([[horariosxactividad objectAtIndex:i] objectForKey:@"etiqueta"] != NULL) {
            [strEntradasxactividad appendString:[[entradasxactividad objectAtIndex:i] objectForKey:@"etiqueta"]];
            [strEntradasxactividad appendString:@"\n"];
        }
        if ([[horariosxactividad objectAtIndex:i] objectForKey:@"texto"] != NULL) {
            [strEntradasxactividad appendString:[[entradasxactividad objectAtIndex:i] objectForKey:@"texto"]];
            [strEntradasxactividad appendString:@"\n\n"];
        }
    }
    
    if (horariosxactividad.count > 0)
        isHorarios = TRUE;
    else
        isHorarios = FALSE;
    
    if (entradasxactividad.count > 0)
        isEntradas = TRUE;
    else
        isEntradas = FALSE;
    
    if (actividad.estacionamientobicicleta == 1 && actividad.estacionamientodiscapacitado == 1) {
        heightEstablecimiento = 215.0;
    }else if (actividad.estacionamientobicicleta == 1 || actividad.estacionamientodiscapacitado == 1) {
        heightEstablecimiento = 130.0;
    }else{
        heightEstablecimiento = 0.0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)btnBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShared:(id)sender {
    [self actionShared];
}

- (IBAction)btnFavorite:(id)sender {
    
    NSInteger idUser = [[Memoria sharedInstance]obtenerUserId];
    
    if (idUser == 0 || idUser == -1) {
        [self AlertDismiss:NSLocalizedString(@"APP_LOGIN_ABOUT", nil)];
        return;
    }

    if ([Utiles isInternetAvailable]) {
        
        NSString *pathURL = nil;
        
        if (actividad.esfavorito == 1) {
            pathURL = @"eliminaractividad";
        }else{
            pathURL = @"grabaractividad";
        }
        
        NSString *post = [NSString stringWithFormat:@"userId=%ld&numlugar=%ld",[[Memoria sharedInstance]obtenerUserId],actividad.codactividad];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@security/%@/%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",pathURL) query:post state:1 withBlock:^(id userInfo, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    if ([dictJson[@"success"]boolValue]) {
                        
                        [self AlertDismiss:actividad.esfavorito == 1 ? NSLocalizedString(@"APP_DELETE_ACTIVITY", nil): NSLocalizedString(@"APP_INSERT_ACTIVITY", nil)];
                        if (actividad.esfavorito == 1) {
                            actividad.esfavorito = 0;
                        }else{
                            actividad.esfavorito = 1;
                        }
                        [tblDetalleActividad reloadData];
                    }
                }
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
}

- (void)actionShared{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"APP_SHARED", nil) delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",
                            @"Twitter",
                            @"WhatsApp",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self FBShareFacebook];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self sharedWhatsApp];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void)sharedWhatsApp {
    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
        
        NSString *Picture = nil;
        
        if (![actividad.txtimagen isBlank]){
            Picture = actividad.txtimagen;
            Picture = [Picture reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        }
        
        UIImage     *iconImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],Picture]];
        NSString    *savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
        
        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
        
        _documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        _documentInteractionController.UTI = @"net.whatsapp.image";
        _documentInteractionController.delegate = self;
        
        [_documentInteractionController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
        
    } else {
        [self SetAlerta:@"Alerta" message:NSLocalizedString(@"APP_INSTALL_WSP", nil)];
    }
}

- (void)FBShareFacebook{
    if(![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])  {
        [Soporte dump:@"log output of your choice here"];
    }
    // Facebook may not be available but the SLComposeViewController will handle the error for us.
    mySLComposerSheet = [[SLComposeViewController alloc] init];
    mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [mySLComposerSheet setInitialText:actividad.txtnombre];
    
    if (![actividad.txtimagen isBlank]){
        NSString *MICONO = actividad.txtimagen;
        MICONO = [MICONO reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        [mySLComposerSheet addImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],MICONO]]]; //an image you could post
    }
    
    [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        }
        if (![output isEqualToString:@"Action Cancelled"]) {
            // Only alert if the post was a success. Or not! Up to you.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)TwitterShare{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *twitter =
        [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [twitter setInitialText:actividad.txtnombre];
        [twitter addURL:[NSURL URLWithString:@"http://www.google.com"]];
        
        if (![actividad.txtimagen isBlank]){
            NSString *MICONO = actividad.txtimagen;
            MICONO = [MICONO reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
            [twitter addImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],MICONO]]]; //an image you could post
        }
        
        [self presentViewController:twitter animated:YES completion:nil];
    } else {
        [Soporte dump:@"it is not configured"];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellMaster = nil;
    
    if (indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"ct01";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        AsyncImageView *thumb = (AsyncImageView *)[cell viewWithTag:300];
        UIImageView *thumbFavorite = (UIImageView *)[cell viewWithTag:302];
        
        [thumbFavorite setImage:[UIImage imageNamed:actividad.esfavorito == 0 ? @"ic_favorite_off.png" : @"ic_favorite_on.png"]];

        NSString *img = actividad.txtimagen;
        
        img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        
        if ([self verificarRutaImagenLocal:img])
            [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        else
            [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,actividad.txtimagen]];
        
        if (!isOver) {
            UIView *overlay = [[UIView alloc] initWithFrame:thumb.frame];
            [overlay setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
            [thumb addSubview:overlay];
            isOver = true;
        }
        
        cellMaster = cell;
    }
    
    if (indexPath.row == 1) {
        
        static NSString *CellIdentifier = @"ct02";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UILabel *fecha = (UILabel *)[cell viewWithTag:306];
        UILabel *nombre = (UILabel *)[cell viewWithTag:307];
        AsyncImageView *thumb = (AsyncImageView *)[cell viewWithTag:308];
        UILabel *institucion = (UILabel *)[cell viewWithTag:309];
        
        NSString *img = actividad.logo;
        
        img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        
        if ([self verificarRutaImagenLocal:img])
            [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        else
            [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,actividad.logo]];
        
        nombre.text = actividad.txtnombre;
        fecha.text = actividad.txthorariosatencion;
        institucion.text = actividad.institucion;
        
        cellMaster = cell;
    }
    if (indexPath.row == 2) {
        
        static NSString *CellIdentifier = @"ct03";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 3) {
        
        static NSString *CellIdentifier = @"ct04";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;
    }
    if (indexPath.row == 4) {
        
        static NSString *CellIdentifier = @"ct05";
        
        DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        
        cellMaster = cell;

    }
    if (indexPath.row == 5) {
        
        static NSString *CellIdentifier = @"ct06";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIImageView *thumbBicicleta = (UIImageView *)[cell viewWithTag:600];
        UILabel *lblBicicleta = (UILabel *)[cell viewWithTag:601];
        
        if (heightEstablecimiento == 130.0) {
            if (actividad.estacionamientobicicleta == 1) {
                [thumbBicicleta setImage:[UIImage imageNamed:@"estacionamientobicicleta"]];
                [lblBicicleta setText:@"Estacionamiento Bicicleta"];
            }else if (actividad.estacionamientodiscapacitado == 1) {
                [thumbBicicleta setImage:[UIImage imageNamed:@"estacionamientodiscapacitado"]];
                [lblBicicleta setText:@"Estacionamiento Discapacitado"];
            }
        }
        
        cellMaster = cell;
        
    }
    if (indexPath.row == 6) {
        
        static NSString *CellIdentifier = @"ct07";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        GMSMapView *_map = (GMSMapView *)[cell viewWithTag:314];
        UILabel *_nombre = (UILabel *)[cell viewWithTag:315];
        
        // Ask for My Location data after the map has already been added to the UI.
        dispatch_async(dispatch_get_main_queue(), ^{
            _map.myLocationEnabled = YES;
        });
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:actividad.numx
                                                                longitude:actividad.numy
                                                                     zoom:kZoom
                                                                  bearing:328.f
                                                             viewingAngle:40.f];
        
        _map.camera = camera;
        _map.settings.compassButton = YES;
        _map.settings.myLocationButton = YES;
        _map.delegate = self;
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(actividad.numx, actividad.numy);
        //marker.snippet = actividad.txtnombre;
        marker.icon = [UIImage imageNamed:@"location"];
        marker.draggable = NO;
        marker.map = _map;
        [_map setSelectedMarker:marker];
        _nombre.text = actividad.txtdireccion;
        
        cellMaster = cell;
    }

    return cellMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat heightDynamic = 0;
    
    if (indexPath.row == 0) {
        heightDynamic =  200.0f;
    }
    if (indexPath.row == 1) {
        heightDynamic =  200.0f;

    }
    if (indexPath.row == 2) {
        static DynamicTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            static NSString *CellIdentifier = @"ct03";
            cell = [self.tblDetalleActividad dequeueReusableCellWithIdentifier:CellIdentifier];
        });
        
        [self setUpCell:cell atIndexPath:indexPath];
        
        heightDynamic = [self calculateHeightForConfiguredSizingCell:cell];
    }
    if (indexPath.row == 3) {
        
        if (isEntradas) {
            static DynamicTableViewCell *cell = nil;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                static NSString *CellIdentifier = @"ct04";
                cell = [self.tblDetalleActividad dequeueReusableCellWithIdentifier:CellIdentifier];
            });
            
            [self setUpCell:cell atIndexPath:indexPath];
            
            heightDynamic = [self calculateHeightForConfiguredSizingCell:cell];
        }else
            heightDynamic = 0.0f;

    }
    if (indexPath.row == 4) {
        
        if (isHorarios) {
            static DynamicTableViewCell *cell = nil;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                static NSString *CellIdentifier = @"ct05";
                cell = [self.tblDetalleActividad dequeueReusableCellWithIdentifier:CellIdentifier];
            });
            
            [self setUpCell:cell atIndexPath:indexPath];
            
            heightDynamic = [self calculateHeightForConfiguredSizingCell:cell];
        }else
            heightDynamic = 0.0f;

    }
    if (indexPath.row == 5) {
        heightDynamic =  heightEstablecimiento;
    }
    if (indexPath.row == 6) {
        heightDynamic =  327.0f;
    }
    
    return heightDynamic;
}

# pragma mark - UITableViewControllerDelegate

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

# pragma mark - Cell Setup

- (void)setUpCell:(DynamicTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    [self.tblDetalleActividad setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([cell.reuseIdentifier isEqualTo:@"ct03"]) {
        cell.label.text = actividad.txtdescripcion;
    }
    if ([cell.reuseIdentifier isEqualTo:@"ct04"]) {
        cell.label.text = strEntradasxactividad;
    }
    if ([cell.reuseIdentifier isEqualTo:@"ct05"]) {
        cell.label.text = strHorarioxactividad;
    }
    
    cell.label.preferredMaxLayoutWidth = self.view.frame.size.width;
}

@end
