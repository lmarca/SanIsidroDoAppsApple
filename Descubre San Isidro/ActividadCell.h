//
//  ActividadCell.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Actividad.h"
#import "AsyncImageView.h"
#import "Util_Color.h"

@interface ActividadCell : UITableViewCell{
    
    IBOutlet AsyncImageView *imgBanner;
    IBOutlet UILabel *lblNombre;
    IBOutlet UILabel *lblInstitucion;
    IBOutlet UILabel *lblDescripcion;
    IBOutlet UILabel *lblDia;
    IBOutlet UILabel *lblMes;
    IBOutlet UIView *content;
    
}

-(void)llenarCelda:(Actividad *)actividad isSucediendo:(BOOL)isSucediendo;

@end
