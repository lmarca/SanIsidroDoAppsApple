//
//  AsyncImageView.h
//  AsyncImageLoad
//
//  Created by Vivek Seth on 1/13/14.
//  Copyright (c) 2014 Vivek Seth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+Utiles.h"
#import "Soporte.h"

//this variable will turn off delay
static BOOL debug = true;

@interface AsyncImageView : UIImageView

@property (nonatomic, strong) NSString * imageURL;

@property (nonatomic, assign) UIActivityIndicatorViewStyle activityIndicatorStyle;
@property (nonatomic, strong) UIActivityIndicatorView *activityView;

@end
