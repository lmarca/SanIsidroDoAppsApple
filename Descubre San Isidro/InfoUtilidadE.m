//
//  InfoUtilidadE.m
//  Turismo San Isidro
//
//  Created by Luis on 20/10/15.
//  Copyright © 2015 DoApps. All rights reserved.
//

#import "InfoUtilidadE.h"

@implementation InfoUtilidadE

@synthesize compra = _compra, venta = _venta, cli_fecha = _cli_fecha, cli_time = _cli_time, cli_cielo = _cli_cielo, cli_humedad = _cli_humedad, cli_presion = _cli_presion, cli_tempmax = _cli_tempmax, cli_tempmin = _cli_tempmin, cli_temperatura = _cli_temperatura;

+ (InfoUtilidadE *)sharedInstance{
    static InfoUtilidadE *_sharedSingleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedSingleton = [[self alloc] init];
    });
    
    return _sharedSingleton;
}

- (id) init{
    if (self = [super init])
    {
        _compra = @"";
        _venta = @"";
        _cli_fecha = @"";
        _cli_time = @"";
        _cli_cielo = @"";
        _cli_humedad = @"";
        _cli_presion = @"";
        _cli_tempmax = @"";
        _cli_tempmin = @"";
        _cli_temperatura = @"";
        // custom initialization
    }
    
    return self;
}

@end
