//
//  ConnectWS.m
//  TuTaxi
//
//  Created by Developer5 on 18/11/14.
//  Copyright © 2016 peru. All rights reserved.
//

#import "ConnectWS.h"

@implementation ConnectWS

+ (void)findAllObjectsWithQuery:(NSString *)url query:(NSString *)query  state:(NSInteger )state withBlock:(void (^)(id userInfo, NSError *error))block
{
  
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    typedef void  (^FetchNextPage)(void);
    FetchNextPage __weak __block weakPointer;
    
    FetchNextPage strongBlock = ^(void)
    {
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.allowsCellularAccess = NO;
        [sessionConfig setHTTPAdditionalHeaders:@{@"Accept": @"application/x-www-form-urlencoded"}];
        sessionConfig.timeoutIntervalForRequest = 30.0;
        sessionConfig.timeoutIntervalForResource = 60.0;
        sessionConfig.HTTPMaximumConnectionsPerHost = 10;
        
        
        NSURL *_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:_url];

        if (state == 0) {
            [request setHTTPMethod:@"GET"];
        }else{
            [request setHTTPMethod:@"POST"];
        }
    
        NSLog(@"URL:%@%@",url,query);
        
        NSError *error = nil;
        NSData *data = [query dataUsingEncoding:NSUTF8StringEncoding];
        
        if (!error) {
            NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                                       fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                           NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                                                                           NSLog(@"Status:%ld",(long)httpResp.statusCode);
                                                                           if (!error && httpResp.statusCode == 200) {
                                                                               id response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                                   options:NSJSONReadingAllowFragments
                                                                                                                                     error:&error];
                                                                               // We are done so return the objects
                                                                               block(response, nil);
                                                                           } else {
                                                                               block(nil,error);
                                                                           }
                                                                       }];
            
            [uploadTask resume];
        }
    };
    
    weakPointer = strongBlock;
    strongBlock();
}

@end
