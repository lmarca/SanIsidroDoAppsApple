//
//  FechaHora.m
//  Demo
//
//  Created by Procesos on 2/02/15.
//  Copyright (c) 2015 com.procesosmc. All rights reserved.
//

#import "FechaHora.h"

static NSString *const FORMATO_HORA=@"HHmmss";

/**
 * Clase utilitaria que permite manejar diversos métodos para obtención de la fecha y hora.
 * @author Luis Marca
 * @version 1.0
 * @date 28/09/2015
**/

@implementation FechaHora

+ (NSString *)formatearHora:(NSInteger)tiempoSegundos{
    
    int segundos = tiempoSegundos % 60;
    int minutos = (tiempoSegundos / 60) % 60;
    //    int horas = tiempoSegundos / 3600;
    
    //    return [NSString stringWithFormat:@"%02d:%02d:%02d",horas, minutos, segundos];
    return [NSString stringWithFormat:@"%02d:%02d", minutos, segundos];
}

+ (NSString *)obtenerHoraActual{
    NSDateFormatter *hora = [[NSDateFormatter alloc] init];
    hora.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [hora setDateFormat:@"HHmmss"];
    NSDate *horaActual = [NSDate date];
    return [hora stringFromDate:horaActual];
}

+ (NSString *)getDate{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //obtenemos la fecha actual
    NSDate *fechaActual = [NSDate date];
    NSString *strFechaActual = [format stringFromDate:fechaActual];
    return strFechaActual;
}

+ (NSString *)obtenerFechaActual{
    NSDateFormatter *fecha = [[NSDateFormatter alloc] init];
    [fecha setDateFormat:@"dd-MM-YYYY"];
    NSDate *fechaActual = [NSDate date];
    return [fecha stringFromDate:fechaActual];
}

+ (NSString *)obtenerFechaActualTransaccion{
    NSString *fechaTransaccion=[self obtenerFechaActual];
    //   return [fechaTransaccion substringFromIndex:4];
    
    NSString *monthDay = [fechaTransaccion substringFromIndex:4];
    NSString *month = [monthDay substringToIndex:2];
    NSString *day = [monthDay substringFromIndex:2];
    
    return [NSString stringWithFormat:@"%@%@", month, day];//confirmed MMdd
}

+ (NSDate *)traefecha{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //obtenemos la fecha actual
    NSDate *fechaActual = [NSDate date];
    NSString *strFechaActual = [format stringFromDate:fechaActual];
    fechaActual = [format dateFromString:strFechaActual];
    return fechaActual;
}

+ (NSString *)getDateForExport{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    NSMutableString *strFechaActual = [NSMutableString string];
    
    [format setDateFormat:@"dd"];
    NSString *myDayString = [format stringFromDate:[NSDate date]];
    
    [format setDateFormat:@"MMM"];
    NSString *myMonthString = [format stringFromDate:[NSDate date]];
    myMonthString = [myMonthString lowercaseString];
    
    [format setDateFormat:@"HHmmss"];
    NSString *hour = [format stringFromDate:[NSDate date]];
    
    [strFechaActual appendString:myDayString];
    [strFechaActual appendFormat:@"%@_",myMonthString];
    [strFechaActual appendFormat:@"%@.pdf",hour];
    
    return strFechaActual;
}

+ (NSInteger )dayFromStringDate:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:dateString];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:dateFromString];
    NSInteger weekday = [comps weekday];
    
    if (weekday > 1) {
        weekday = weekday - 2;
    }else{
        weekday = 6;
    }
    
    return weekday;
}


@end
