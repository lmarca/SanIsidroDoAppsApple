//
//  ListadoLugaresVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 4/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventoCell.h"
#import "Lugar.h"
#import "Evento.h"
#import "myActivity.h"

@interface ListadoLugaresVC : myActivity<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic,strong) Lugar *lugar;

@end
