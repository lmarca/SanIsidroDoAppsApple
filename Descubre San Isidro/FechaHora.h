//
//  FechaHora.h
//  Demo
//
//  Created by Procesos on 2/02/15.
//  Copyright (c) 2015 com.procesosmc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FechaHora : NSObject

+ (NSString *)formatearHora:(NSInteger)tiempoSegundos;
+ (NSString *)obtenerHoraActual;
+ (NSString *)obtenerFechaActual;
+ (NSString *)obtenerFechaActualTransaccion;
+ (NSString *)getDate;
+ (NSDate *)traefecha;
+ (NSString *)getDateForExport;
+ (NSInteger )dayFromStringDate:(NSString *)dateString;


@end
