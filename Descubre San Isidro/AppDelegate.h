//
//  AppDelegate.h
//  Descubre San Isidro
//
//  Created by Luis on 30/08/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Util_Color.h"
#import "Constantes.h"
#import "LoginPrincipalVC.h"
#import <Google/SignIn.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, readonly, retain) UITabBarController *tabBarController;
@property(nonatomic, retain) UIStoryboard *mainStoryboard;


- (void)LoadingOn:(NSString *)msg;
- (void)LoadingOff;
- (void)loadTabBar;


@end

