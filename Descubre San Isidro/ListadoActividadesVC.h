//
//  ListadoActividadesVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 4/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActividadCell.h"
#import "DetalleActividadVC.h"
#import "DetalleLugarVC.h"
#import "LugarCell.h"
#import "myActivity.h"
#import "Lugar.h"
#import "Actividad.h"
#import "Menuportadalugar.h"
#import "MenuPortadaactividad.h"

@interface ListadoActividadesVC : myActivity<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property(nonatomic,strong) Actividad *actividad;

@property(nonatomic) BOOL isActividad;
@property (weak, nonatomic) IBOutlet UIView *vTopBar;

@property (nonatomic) MenuPortadaactividad *objMenuActividad;
@property (nonatomic) Menuportadalugar *objMenu;
@property (nonatomic) Lugar *lugar;
@property (strong, nonatomic) IBOutlet UITableView *tblGenerico;
@property (strong, nonatomic) IBOutlet UISearchBar *search;


@end
