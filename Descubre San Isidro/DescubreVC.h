//
//  DescubreVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginPrincipalVC.h"
#import "ActividadCell.h"
#import "LugarCell.h"
#import "ListadoLugaresVC.h"
#import "DetalleActividadVC.h"
#import "ListadoActividadesVC.h"
#import "myActivity.h"
#import "Util_Color.h"
#import "AsyncImageView.h"
#import "Actividad.h"
#import "CellMenuLug.h"

#import "Menuportadalugar.h"
#import "MenuPortadaactividad.h"
#import "CollectionViewCell.h"

@interface DescubreVC : myActivity <UIScrollViewDelegate , UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsHeightCt;

@property(nonatomic,strong) Actividad *actividad;
@property (nonatomic) Menuportadalugar *objMenu;
@property (nonatomic) MenuPortadaactividad *objMenuActividad;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,strong) CollectionViewCell *celda;


@end
