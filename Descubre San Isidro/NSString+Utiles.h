//
//  NSString+Utiles.h
//  MPOS
//
//  Created by Procesos on 19/08/15.
//  Copyright (c) 2015 com.procesosmc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utiles)

- (BOOL)isBlank;
- (BOOL)isEqualTo:(NSString *)otherString;
- (BOOL)contains:(NSString*)string;
- (BOOL)containsOnlyLetters;
- (BOOL)containsOnlyNumbers;
- (BOOL)containsOnlyNumbersAndLetters;
- (NSArray *)splitOnChar:(char)ch;
- (NSString *)stringByStrippingWhitespace;
- (NSString *)reemplazarCaracteresCadena:(NSString *)caracterAntiguo
                        conNuevoCaracter:(NSString *)caracterNuevo;
- (NSString*)addString:(NSString*)string;
- (BOOL)isPrefix:(NSString *)otherString;
- (BOOL)isSuffix:(NSString *)otherString;

@end
