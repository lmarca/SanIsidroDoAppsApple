//
//  AppDelegate.m
//  SanIsidroApp
//
//  Created by Luis on 26/04/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "AppDelegate.h"
#import "LoadingView.h"
#import "ViewController.h"
#import "SanIsidroVC.h"
#import "DescubreVC.h"
#import "UtilidadesVC.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface AppDelegate ()

@property NSMutableArray *Array;

@end

@implementation AppDelegate

static LoadingView *vLoading;

// DO NOT USE THIS CLIENT ID. IT WILL NOT WORK FOR YOUR APP.
// Please use the client ID created for you by Google.
static NSString * const kClientID =
@"875987554065-qiir8hu1c4kd50tqq92qlscolfg5h55a.apps.googleusercontent.com";

@synthesize tabBarController, mainStoryboard;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSString * language = [NSLocale preferredLanguages][0]; //es-PE
    [Soporte dump:language];
    [NSStandardUserDefaults saveObject:language forKey:kIdioma];
    
    //sleep(3);
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    [GMSServices provideAPIKey:kAPIKey];
    
    [GIDSignIn sharedInstance].clientID = kClientID;
    
    [GIDSignIn sharedInstance].delegate = self;
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    if(![NSStandardUserDefaults boolForKey:Sesion]){
        LoginPrincipalVC *vc = (LoginPrincipalVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"LoginPrincipalVC"];
        self.window.rootViewController = vc;
    }else{
        [self loadTabBar];
    }
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor grayColor] }
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0] }
                                             forState:UIControlStateSelected];
    
    return YES;
}

- (void)loadTabBar {
    
    ViewController *vc01 = (ViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
    UINavigationController *nav01 = [[UINavigationController alloc]initWithRootViewController:vc01];
    [nav01 setNavigationBarHidden:YES animated:NO];
    nav01.navigationItem.hidesBackButton = true;
    
    SanIsidroVC *vc02 = (SanIsidroVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"SanIsidroVC"];
    UINavigationController *nav02 = [[UINavigationController alloc]initWithRootViewController:vc02];
    [nav02 setNavigationBarHidden:YES animated:NO];
    nav02.navigationItem.hidesBackButton = true;
    
    DescubreVC *vc03 = (DescubreVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"DescubreVC"];
    UINavigationController *nav03 = [[UINavigationController alloc]initWithRootViewController:vc03];
    [nav03 setNavigationBarHidden:YES animated:NO];
    nav03.navigationItem.hidesBackButton = true;
    
    UtilidadesVC *vc04 = (UtilidadesVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"UtilidadesVC"];
    UINavigationController *nav04 = [[UINavigationController alloc]initWithRootViewController:vc04];
    [nav04 setNavigationBarHidden:YES animated:NO];
    nav04.navigationItem.hidesBackButton = true;
    
    tabBarController = [[UITabBarController alloc] init];
    self.Array = [NSMutableArray arrayWithArray:@[nav01, nav02, nav03, nav04]];
    if (![NSStandardUserDefaults boolForKey:Sesion]) {
        LoginPrincipalVC *firstvc = (LoginPrincipalVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"LoginPrincipalVC"];
        UINavigationController *firstNav = [[UINavigationController alloc]initWithRootViewController:firstvc];
        [firstNav setNavigationBarHidden:YES animated:NO];
        [self.Array replaceObjectAtIndex:0 withObject:firstNav];
    }
    
    tabBarController.viewControllers = self.Array;
    tabBarController.delegate = self;
    tabBarController.tabBar.translucent = NO;
    self.window.rootViewController = tabBarController;
    
    [tabBarController.viewControllers[0].tabBarItem setSelectedImage:[[UIImage imageNamed:@"menu_miperfil_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    [tabBarController.viewControllers[0].tabBarItem setImage:[[UIImage imageNamed:@"menu_miperfil"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    tabBarController.viewControllers[0].tabBarItem.title = [Memoria sharedInstance].isEspanol? @"Mi Perfil" : @"My Profile";
    
    [tabBarController.viewControllers[1].tabBarItem setSelectedImage:[[UIImage imageNamed:@"menu_sanisidro_selected"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    [tabBarController.viewControllers[1].tabBarItem setImage:[[UIImage imageNamed:@"menu_sanisidro"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    tabBarController.viewControllers[1].tabBarItem.title = @"San Isidro";
    
    [tabBarController.viewControllers[2].tabBarItem setSelectedImage:[[UIImage imageNamed:@"menu_descubre_selected"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    [tabBarController.viewControllers[2].tabBarItem setImage:[[UIImage imageNamed:@"menu_descubre"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    tabBarController.viewControllers[2].tabBarItem.title = [Memoria sharedInstance].isEspanol? @"Descubre" : @"Discover";
    
    [tabBarController.viewControllers[3].tabBarItem setSelectedImage:[[UIImage imageNamed:@"menu_utilidades_selected"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    [tabBarController.viewControllers[3].tabBarItem setImage:[[UIImage imageNamed:@"menu_utilidades"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ]];
    tabBarController.viewControllers[3].tabBarItem.title = [Memoria sharedInstance].isEspanol? @"Utilidades" : @"Utilities";
    
    tabBarController.selectedIndex = 2;
}

//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary *)options {
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    
    [[GIDSignIn sharedInstance]handleURL:url
                       sourceApplication:sourceApplication
                              annotation:annotation];
    
    return YES;
}

- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    
    if (indexOfTab == 0) {
        
        if (![NSStandardUserDefaults boolForKey:Sesion]) {
            tabBarController.tabBar.translucent = YES;
            self.tabBarController.tabBar.hidden = YES;
        }else{
            tabBarController.tabBar.translucent = NO;
            self.tabBarController.tabBar.hidden = NO;
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
}

#pragma mark - GPPDeepLinkDelegate

//- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink{
//    // An example to handle the deep link data.
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@"Deep-link Data"
//                          message:[deepLink deepLinkID]
//                          delegate:nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil];
//    [alert show];
//}

// [START signin_handler]
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *email = user.profile.email;
    // [START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText":
                                     [NSString stringWithFormat:@"Signed in user: %@",
                                      fullName]};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}
// [END signin_handler]

// This callback is triggered after the disconnect call that revokes data
// access to the user's resources has completed.
// [START disconnect_handler]
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // [START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}
// [END disconnect_handler]

- (void)LoadingOn:(NSString *)msg{
    dispatch_async(dispatch_get_main_queue(),^(void){
        if (!vLoading)
            vLoading = [LoadingView new];
        
        [self.window addSubview:vLoading];
        [vLoading loadViewsWithMessage:msg];
        [vLoading startLoading];
    });
}

- (void)LoadingOff{
    dispatch_async(dispatch_get_main_queue(),^(void){
        [vLoading stopLoading];
        vLoading = NULL;
    });
}

@end
