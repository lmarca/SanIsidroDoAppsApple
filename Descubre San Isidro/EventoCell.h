//
//  EventoCell.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 5/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Evento.h"

@interface EventoCell : UITableViewCell{
    IBOutlet UIImageView *imgBanner;
    IBOutlet UILabel *lblCreador;
    IBOutlet UILabel *lblDireccion;
}

-(void)llenarCelda:(Evento *)evento;

@end
