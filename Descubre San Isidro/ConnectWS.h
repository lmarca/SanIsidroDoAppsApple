//
//  ConnectWS.h
//  TuTaxi
//
//  Created by Developer5 on 18/11/14.
//  Copyright © 2016 peru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectWS : NSObject

+ (void)findAllObjectsWithQuery:(NSString *)url query:(NSString *)query  state:(NSInteger )state withBlock:(void (^)(id userInfo, NSError *error))block;


@end