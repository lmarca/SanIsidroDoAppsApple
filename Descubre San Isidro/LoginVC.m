//
//  LoginVC.m
//  SanIsidroApp
//
//  Created by Luis on 2/05/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "LoginVC.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface LoginVC (){
    
    IBOutlet UITextField *txtUser;
    IBOutlet UITextField *txtPass;
    
    IBOutlet UILabel *lblTexto;
}

@property(readwrite)int  lastTag;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLeftPadding:05 textField:txtUser];
    [self setLeftPadding:05 textField:txtPass];
    
    txtUser.layer.borderColor=[[UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0] CGColor];
    txtUser.layer.borderWidth=1.0;
    
    txtPass.layer.borderColor=[[UIColor colorWithRed:169.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0] CGColor];
    txtPass.layer.borderWidth=1.0;
    
    _lastTag = 1;
    
    lblTexto.text = @"Versión 1.2\nMunicipalidad de San Isidro\nLima - Perú";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField.tag == _lastTag) textField.returnKeyType = UIReturnKeyDone;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger tag = textField.tag;
    if(textField.tag == _lastTag)
    {
        [textField resignFirstResponder];
        return YES;
        
    } else {
        UITextField *target = (UITextField *)[self.view viewWithTag:tag+1];
        [target becomeFirstResponder];
        return NO;
    }
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self animatedTextField:textField up:NO];
    [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self animatedTextField:textField up:YES];
}

- (void)animatedTextField:(UITextField *)textField up:(BOOL)up{
    int animatedDistance;
    int moveUpValue;
    if (IS_IPHONE4)
        moveUpValue =textField.frame.origin.y + textField.frame.size.height+50;
    else if (IS_IPHONE5)
        moveUpValue =textField.frame.origin.y + textField.frame.size.height-10;
    else if (IS_IPHONE6)
        moveUpValue =textField.frame.origin.y + textField.frame.size.height-105;
    else
        moveUpValue =textField.frame.origin.y + textField.frame.size.height-160;
    
    animatedDistance=216-(500-moveUpValue-5);
    if (animatedDistance>0) {
        const int movementDistance =animatedDistance;
        const float movementDuration=0.3;
        int movement=(up ? -movementDistance:movementDistance);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:movementDuration];
        self.view.frame=CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

- (IBAction)backTo:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)onClickInput:(id)sender {
    
    if ([Utiles isInternetAvailable]) {
        
        if(![Soporte validEmail:txtUser.text regExPattern:@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$"]) {
            [self AlertDismiss:NSLocalizedString(@"APP_VALIDATE_MAIL", nil)];
            return;
        }
        
        if (txtUser.hasText && txtPass.hasText) {
            if(txtUser.isFirstResponder) [txtUser resignFirstResponder];
            if(txtPass.isFirstResponder) [txtPass resignFirstResponder];
            
            [self LoadingOn:NSLocalizedString(@"APP_LOG_IN", nil)];
            
            [ConnectWS findAllObjectsWithQuery:f(@"%@%@,%@%@",urlWS,txtUser.text,txtPass.text,@"/validarusuario") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                               {
                                   if (userInfo) {
                                       [Soporte dump:@"" prefijo:userInfo];
                                       
                                       [self LoadingOff];
                                       
                                       NSDictionary *dictJson = (NSDictionary *)userInfo;
                                       NSArray *arrayJson = dictJson[@"data"];
                                       
                                       if (arrayJson.count != 0) {
                                           for(int i=0;i<arrayJson.count;i++)
                                           {
                                               [NSStandardUserDefaults saveObject:[[arrayJson objectAtIndex:i] objectForKey:@"ba_user_id"] forKey:ba_user_id];
                                               [NSStandardUserDefaults saveObject:[[arrayJson objectAtIndex:i] objectForKey:@"email"] forKey:email];
                                               [NSStandardUserDefaults saveObject:[[arrayJson objectAtIndex:i] objectForKey:@"name"]forKey:name];
                                               [NSStandardUserDefaults saveObject:[[arrayJson objectAtIndex:i] objectForKey:@"login_user"] forKey:login_user];
                                               [NSStandardUserDefaults saveBool:false forKey:isRedesSociales];
                                               [NSStandardUserDefaults saveBool:true forKey:Sesion];
                                           }
                                       }else
                                           [self AlertDismiss:NSLocalizedString(@"USER_INVALIDATE", nil)];
                                       
                                   }
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if ([NSStandardUserDefaults boolForKey:Sesion]) {
                                           AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                           [appDelegate loadTabBar];
                                       }
                                   });
                               });
            }];
        }
        else [self SetAlerta:nil message:NSLocalizedString(@"APP_FIELD_ENTER", nil)];
    }
    else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}

- (IBAction)onIrApp:(id)sender {
}


@end
