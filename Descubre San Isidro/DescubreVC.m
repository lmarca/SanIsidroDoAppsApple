//
//  DescubreVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "DescubreVC.h"


@interface sldActividad : NSObject

@property NSString *codactividad, *txtimagen;

+ (sldActividad *)newActCodactividad:(NSString *)codactividad txtimagen:(NSString *)txtimagen;

@end

@implementation sldActividad

+ (sldActividad *)newActCodactividad:(NSString *)codactividad txtimagen:(NSString *)txtimagen {
    sldActividad *act = [sldActividad new];
    [act setCodactividad:codactividad];
    [act setTxtimagen:txtimagen];
    return act;
}

@end

@interface sldLugar : NSObject

@property NSString *codlugar, *txtimagen;

+ (sldLugar *)newActCodlugar:(NSString *)codlugar txtimagen:(NSString *)txtimagen;

@end

@implementation sldLugar

+ (sldLugar *)newActCodlugar:(NSString *)codlugar txtimagen:(NSString *)txtimagen {
    sldLugar *act = [sldLugar new];
    [act setCodlugar:codlugar];
    [act setTxtimagen:txtimagen];
    return act;
}

@end

@interface DescubreVC (){
    
    IBOutlet UIView *vTopBar;
    IBOutlet UISegmentedControl *segmentControl;
    IBOutlet UITableView *tbGenerico;
    
    NSArray *menu;
    
    NSMutableArray *menuActividades;
    NSMutableArray *listaActividades;
    NSMutableArray *listaLugares;
    NSMutableArray *listaSegLugares;
    NSMutableDictionary *dictSegLugares;
    
    NSMutableArray *dataSlideAct;
    NSMutableArray *dataSlideLug;
    
    BOOL isActividad;
    NSString *opcion;
    
    UIPageControl *pageControl;
    UIScrollView *scroll;
    NSMutableArray *dataSource;
    
    NSMutableArray *listaEntradasxactividad;
    NSMutableArray *listaHorariosxactividad;
    NSMutableArray *listaTextopresentacionxtipoactividad;
}

@end

@implementation DescubreVC

@synthesize scrollView, nsHeight, objMenu, actividad, nsHeightCt, objMenuActividad, celda;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 0, 0, 0);
    layout.minimumLineSpacing = 20;
    layout.minimumInteritemSpacing = 0;
    
    if (IS_IPHONE4 || IS_IPHONE5) {
        layout.itemSize = CGSizeMake(65, 74);
    }else
        layout.itemSize = CGSizeMake(70, 74);
    
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    
    _collectionView.collectionViewLayout = layout;
    
    [_collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CellActividades"];
    [_collectionView setBackgroundColor:[UIColor whiteColor]];

    
    isActividad = YES;
    dictSegLugares = [NSMutableDictionary dictionary];
    
    objMenu = [[Menuportadalugar alloc]init];
    
    menuActividades = [NSMutableArray array];
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
    [ConnectWS findAllObjectsWithQuery:f(@"%@%@%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",@"/menuportadaactividades")
                                 query:@""
                                 state:0
                             withBlock:^(id userInfo, NSError *error) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (userInfo) {
                                         [Soporte dump:@"" prefijo:userInfo];
                                         
                                         NSDictionary *dictJson = (NSDictionary *)userInfo;
                                         NSArray *arrayJson = dictJson[@"data"];
                                         if (arrayJson.count > 0) {
                                             for(int i=0;i<arrayJson.count;i++)
                                             {
                                                objMenuActividad = [[MenuPortadaactividad alloc]init];
                                                 
                                                [objMenuActividad setNumsubmenu:[[[arrayJson objectAtIndex:i] objectForKey:@"numsubmenu"] integerValue]];
                                                [objMenuActividad setCodactividadtipo:[[[arrayJson objectAtIndex:i] objectForKey:@"codactividadtipo"] integerValue]];
                                                [objMenuActividad setCodsubmenufinal:[[[arrayJson objectAtIndex:i] objectForKey:@"codsubmenufinal"] integerValue]];
                                                 
                                                 [objMenuActividad setTxticono:[[arrayJson objectAtIndex:i] objectForKey:@"txticono"]];
                                                 [objMenuActividad setTxtnombre:[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"]];
                                                 
                                                 [menuActividades addObject:objMenuActividad];
                                             }
                                             [_collectionView reloadData];
                                         }
                                     }
                                 });
                             }];
    

    menu = [[NSArray alloc] initWithObjects:@"Cine", @"Música", @"Exposiciones", @"Teatro", @"Talleres", @"Danza", @"Turismo", @"Lectura", nil];
        
    [ConnectWS findAllObjectsWithQuery:f(@"%@%@%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",@"/imagenesactividadesportada")
                                 query:@""
                                 state:0
                             withBlock:^(id userInfo, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (userInfo) {
                [Soporte dump:@"" prefijo:userInfo];
                
                NSDictionary *dictJson = (NSDictionary *)userInfo;
                dataSlideAct = [NSMutableArray array];
                
                NSArray *arrayJson = dictJson[@"data"];
                if (arrayJson.count > 0) {
                    for(int i=0;i<arrayJson.count;i++)
                    {
                        if ([[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]) {
                            [dataSlideAct addObject:[[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]];
                        }
                    }
                    [self onLoadActividadesSlide];
                }
            }
        });
    }];
    
        
    [ConnectWS findAllObjectsWithQuery:f(@"%@%@%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",@"/imageneslugaresportada")
                                 query:@""
                                 state:0
                             withBlock:^(id userInfo, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (userInfo) {
                [Soporte dump:@"" prefijo:userInfo];
                
                NSDictionary *dictJson = (NSDictionary *)userInfo;
                dataSlideLug = [NSMutableArray array];
                
                NSArray *arrayJson = dictJson[@"data"];
                if (arrayJson.count > 0) {
                    for(int i=0;i<arrayJson.count;i++)
                    {
                        if ([[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]) {
                            [dataSlideLug addObject:[[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]];
                        }
                    }
                }
            }
        });
    }];
    
    listaSegLugares = [NSMutableArray array];
    
    [ConnectWS findAllObjectsWithQuery:f(@"%@%@%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",@"/menuportadalugar/principal")
                                 query:@""
                                 state:0
                             withBlock:^(id userInfo, NSError *error) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (userInfo) {
                                         [Soporte dump:@"" prefijo:userInfo];
                                         
                                         NSDictionary *dictJson = (NSDictionary *)userInfo;
                                         
                                         NSDictionary *xyz = dictJson[@"data"];
                                         
                                         [listaSegLugares addObjectsFromArray:[xyz allKeys]];
                                         
                                         if (xyz.count > 0) {
                                             for (NSString *key in xyz) {
                                                 NSArray *value = [xyz objectForKey:key];
                                                 
                                                 NSMutableArray *cargaData = [NSMutableArray array];
                                                 for(int i=0;i<value.count;i++)
                                                 {
                                                     objMenu = [Menuportadalugar new];
                                                     objMenu.codsubmenufinal = [[[value objectAtIndex:i] objectForKey:@"codsubmenufinal"]integerValue];
                                                     objMenu.numlugartipo = [[[value objectAtIndex:i] objectForKey:@"numlugartipo"]integerValue];
                                                     objMenu.numsubmenu = [[[value objectAtIndex:i] objectForKey:@"numsubmenu"]integerValue];
                                                     objMenu.txticono = [[value objectAtIndex:i] objectForKey:@"txticono"];
                                                     objMenu.txtnombre = [[value objectAtIndex:i] objectForKey:@"txtnombre"];
                                                     
                                                     [cargaData addObject:objMenu];
                                                 }
                                                 
                                                 [dictSegLugares setObject:cargaData forKey:key];
                                             }
                                         }
                                     }
                                 });
                             }];
    
    
    listaActividades = [[NSMutableArray alloc] init];
    actividad = [Actividad new];
    
    [ConnectWS findAllObjectsWithQuery:f(@"%@%@%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",@"/sucediendoahora/get")
                                 query:@""
                                 state:0
                             withBlock:^(id userInfo, NSError *error) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (userInfo) {
                                         [Soporte dump:@"" prefijo:userInfo];
                                         
                                         NSDictionary *dictJson = (NSDictionary *)userInfo;
                                         NSArray *arrayJson = dictJson[@"data"];
                                         if (arrayJson.count > 0) {
                                             for(int i=0;i<arrayJson.count;i++)
                                             {
                                                 [actividad setCodactividad:[[[arrayJson objectAtIndex:i] objectForKey:@"codactividad"] integerValue]];
                                                 [actividad setTiempodeiniciado:[[arrayJson objectAtIndex:i] objectForKey:@"tiempodeiniciado"]];
                                                 [actividad setTxtimagen:[[arrayJson objectAtIndex:i] objectForKey:@"txtimagen"]];
                                                 [actividad setInstitucion:[[arrayJson objectAtIndex:i] objectForKey:@"txtinstitucion"]];
                                                 [actividad setTxtnombre:[[arrayJson objectAtIndex:i] objectForKey:@"txtnombre"]];
                                                 
                                                 [listaActividades addObject:actividad];
                                             }
                                             [tbGenerico reloadData];
                                         }
                                     }
                                 });
                             }];
    
    });
    
    nsHeight.constant = 186;
    nsHeightCt.constant = 614;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return menuActividades.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellActividades";
    
    celda = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    objMenuActividad = (MenuPortadaactividad *)[menuActividades objectAtIndex:indexPath.row];
    
    [celda.button setTag:indexPath.row];
    [celda.button addTarget:self action:@selector(actDetalle:) forControlEvents:UIControlEventTouchUpInside];
    
    [celda llenarCelda:objMenuActividad];
    
    return celda;
}


-(void)actDetalle:(UIButton *)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_collectionView];
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        objMenuActividad = (MenuPortadaactividad *)[menuActividades objectAtIndex:indexPath.row];
        
        NSLog(@"%ld %@",objMenuActividad.codactividadtipo, objMenuActividad.txtnombre);
        
        ListadoActividadesVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ListadoActividadesVC"];
        if (!isActividad) {
            vc.objMenu = objMenu;
        }else
            vc.objMenuActividad = objMenuActividad;
        
        vc.isActividad = isActividad;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)onLoadLugaresSlide{

    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180)];
    scroll.backgroundColor = [UIColor whiteColor];
    scroll.delegate = self;
    scroll.pagingEnabled = YES;
    
    [scroll setContentSize:CGSizeMake(scroll.frame.size.width*dataSlideLug.count -1, scroll.frame.size.height)];
    
    // page control
    pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 140, self.content.frame.size.width, 36)];
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.numberOfPages = dataSlideLug.count;
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0];
    [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
    CGFloat x=0;
    
    for (NSString *item in dataSlideLug) {
        
        AsyncImageView *imgBanner = [[AsyncImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, 180)];
        [imgBanner setBackgroundColor:[UIColor lightGrayColor]];
        
        NSString *img = [item reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        
        if ([self verificarRutaImagenLocal:img]) {
            [imgBanner setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
            [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
        }else{
            [imgBanner setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,item]];
            [Soporte dump:@"CARGANDO LA IMAGEN DESDE LA WEB"];
        }
        
        [scroll addSubview:imgBanner];
        
        x+=self.view.frame.size.width;
    }
    
    [self.content addSubview:scroll];
    [scroll addSubview:pageControl];
    [self.content addSubview:pageControl];
}

- (void)onLoadActividadesSlide{

    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 180)];
    scroll.backgroundColor = [UIColor whiteColor];
    scroll.delegate = self;
    scroll.pagingEnabled = YES;
    
    [scroll setContentSize:CGSizeMake(scroll.frame.size.width*dataSlideAct.count -1, scroll.frame.size.height)];
    
    // page control
    pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 140, self.content.frame.size.width, 36)];
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.numberOfPages = dataSlideAct.count;
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"9E0053" andAlpha:1.0];
    [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
    CGFloat x=0;
    
    for (NSString *item in dataSlideAct) {
        
        AsyncImageView *imgBanner = [[AsyncImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, 180)];
        [imgBanner setBackgroundColor:[UIColor lightGrayColor]];
        
        NSString *img = [item reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
        
        if ([self verificarRutaImagenLocal:img]) {
            [imgBanner setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
            [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
        }else{
            [imgBanner setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,item]];
            [Soporte dump:@"CARGANDO LA IMAGEN DESDE LA WEB"];
        }
        
        [scroll addSubview:imgBanner];
        
        x+=self.view.frame.size.width;
    }
    
    [self.content addSubview:scroll];
    [scroll addSubview:pageControl];
    [self.content addSubview:pageControl];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
    CGFloat viewWidth = _scrollView.frame.size.width;
    NSInteger pageNumber = floor((_scrollView.contentOffset.x - viewWidth/50) / viewWidth) +1;
    pageControl.currentPage=pageNumber;
    
}

- (void)pageChanged {
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*pageControl.currentPage;
    frame.origin.y=0;
    
    [scroll scrollRectToVisible:frame animated:YES];
}

- (IBAction)actiionSegmentIndex:(id)sender{
    //
    if(segmentControl.selectedSegmentIndex==0){
        @try {
            vTopBar.backgroundColor = [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0];
            isActividad = YES;
            [self onLoadActividadesSlide];
            nsHeight.constant = 186;
            nsHeightCt.constant = 614;
        } @catch (NSException *exception) {
            NSLog(@"Exception: %@",exception.description);
        } @finally {
            [tbGenerico reloadData];
        }
    }
    else{

        @try {
            vTopBar.backgroundColor = [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0];
            isActividad = NO;
            [self onLoadLugaresSlide];
            nsHeight.constant = 0;
            nsHeightCt.constant = 747;
        } @catch (NSException *exception) {
            NSLog(@"Exception: %@",exception.description);
        } @finally {
            [tbGenerico reloadData];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return (isActividad)?1:dictSegLugares.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isActividad) return listaActividades.count;
    else{
        NSString *sectionTitle = [listaSegLugares objectAtIndex:section];
        NSArray *sectionAnimals = [dictSegLugares objectForKey:sectionTitle];
        return [sectionAnimals count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(isActividad){
        if (listaActividades.count > 0) {
            return NSLocalizedString(@"APP_SUCESS_NOW", nil);
        }
        return @"";
    }else {
        return [listaSegLugares objectAtIndex:section];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (isActividad) {
        return nil;
    }
    else{
        UIView *vHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 60)];
        vHeader.backgroundColor = [UIColor colorWithHexString:@"e5e5e5" andAlpha:1.0];
        UILabel *lblTitle = [[UILabel alloc] init];
        lblTitle.text = [listaSegLugares objectAtIndex:section];
        lblTitle.textColor = [UIColor colorWithHexString:@"818181" andAlpha:1.0];
        lblTitle.frame = CGRectMake(20, 30, vHeader.frame.size.width, 20);
        lblTitle.font = [UIFont fontWithName:@"Arial" size:13];
        [vHeader addSubview:lblTitle];
        return vHeader;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(isActividad) return 30;
    else return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isActividad) return 220;
    else return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isActividad) {
        static NSString *cellIdentifier = @"ActividadCell";
        ActividadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        if (cell==nil) {
            cell = [[ActividadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        Actividad *ac = listaActividades[indexPath.row];
        [cell llenarCelda:ac isSucediendo:true];
        return cell;
    }
    else{
        
        
        static NSString *cellIdentifier = @"CellMenuLug";
        CellMenuLug *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell==nil) {
            cell = [[CellMenuLug alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSString *sectionTitle = [listaSegLugares objectAtIndex:indexPath.section];
        NSArray *sectionAnimals = [dictSegLugares objectForKey:sectionTitle];
        objMenu = (Menuportadalugar *)[sectionAnimals objectAtIndex:indexPath.row];
        
        [cell.button setTag:indexPath.row];
        [cell.button addTarget:self action:@selector(actEditar:) forControlEvents:UIControlEventTouchUpInside];
        [cell llenarCelda:objMenu];
        return cell;

    }
}

-(void)actEditar:(UIButton *)sender{

    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tbGenerico];
    NSIndexPath *indexPath = [tbGenerico indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSString *sectionTitle = [listaSegLugares objectAtIndex:indexPath.section];
        NSArray *sectionAnimals = [dictSegLugares objectForKey:sectionTitle];
        objMenu = (Menuportadalugar *)[sectionAnimals objectAtIndex:sender.tag];
        //[self performSegueWithIdentifier:@"segueBotones" sender:self];
        
        ListadoActividadesVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ListadoActividadesVC"];
        vc.objMenu = objMenu;
        vc.isActividad = isActividad;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    actividad = listaActividades[indexPath.row];
    [self llenarDetalleActividad:actividad.codactividad];
}

- (void)llenarDetalleActividad: (NSInteger)codactividad{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@%@%ld,%ld%@",urlWS,[Memoria sharedInstance].isEspanol?@",":@"en,",(long)codactividad,[[Memoria sharedInstance]obtenerUserId],@"/actividad|horariosxactividad|entradasxactividad|textopresentacionxtipoactividad/multiple") query:@"" state:0 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (userInfo) {
                    [Soporte dump:@"" prefijo:userInfo];
                    
                    NSDictionary *dictJson = (NSDictionary *)userInfo;
                    
                    NSDictionary *dictActividad = dictJson[@"actividad"];
                    NSArray *arrayActividad = dictActividad[@"data"];
                    
                    for (int i = 0; i<arrayActividad.count; i++) {
                        actividad = [Actividad new];
                        [actividad setCodactividad:[[[arrayActividad objectAtIndex:i] objectForKey:@"codactividad"] integerValue]];
                        [actividad setNumx:[[[arrayActividad objectAtIndex:i] objectForKey:@"numx"] floatValue]];
                        [actividad setNumy:[[[arrayActividad objectAtIndex:i] objectForKey:@"numy"] floatValue]];
                        [actividad setTxtdireccion:[[arrayActividad objectAtIndex:i] objectForKey:@"txtdireccion"]];
                        [actividad setTxthorariosatencion:[[arrayActividad objectAtIndex:i] objectForKey:@"txthorariosatencion"]];
                        [actividad setTxtimagen:[[arrayActividad objectAtIndex:i] objectForKey:@"txtimagen"]];
                        [actividad setTxtnombre:[[arrayActividad objectAtIndex:i] objectForKey:@"txtnombre"]];
                        [actividad setEsfavorito:[[[arrayActividad objectAtIndex:i] objectForKey:@"esfavorito"] integerValue]];
                    }
                    
                    NSDictionary *dictEntradasxactividad = dictJson[@"entradasxactividad"];
                    listaEntradasxactividad = [[NSMutableArray alloc]initWithArray:dictEntradasxactividad[@"data"]];
                    NSDictionary *dictHorariosxactividad = dictJson[@"horariosxactividad"];
                    listaHorariosxactividad = [[NSMutableArray alloc]initWithArray:dictHorariosxactividad[@"data"]];
                    NSDictionary *dictTextopresentacionxtipoactividad = dictJson[@"textopresentacionxtipoactividad"];
                    listaTextopresentacionxtipoactividad = [[NSMutableArray alloc]initWithArray:dictTextopresentacionxtipoactividad[@"data"]];
                    
                    if (listaTextopresentacionxtipoactividad.count > 0) {
                        [actividad setLogo:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"logo"]];
                        [actividad setInstitucion:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"institucion"]];
                        [actividad setTexto:[[listaTextopresentacionxtipoactividad objectAtIndex:0] objectForKey:@"texto"]];
                    }else{
                        [actividad setLogo:@""];
                        [actividad setInstitucion:@""];
                        [actividad setTexto:@""];
                    }
                    
                    DetalleActividadVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetalleActividadVC"];
                    vc.actividad = actividad;
                    vc.entradasxactividad = listaEntradasxactividad;
                    vc.horariosxactividad = listaHorariosxactividad;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
    
}


@end
