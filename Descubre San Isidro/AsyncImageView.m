//
//  AsyncImageView.m
//  AsyncImageLoad
//
//  Created by Vivek Seth on 1/13/14.
//  Copyright (c) 2014 Vivek Seth. All rights reserved.
//

#import "AsyncImageView.h"

@implementation AsyncImageView

@synthesize imageURL = _imageURL;

- (void) setImageURL:(NSString *)imageURL {
    
    if (self.activityView == nil)
    {
        self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:self.activityIndicatorStyle];
        self.activityView.hidesWhenStopped = YES;
        self.activityView.center = CGPointMake(self.bounds.size.width / 2.0f, self.bounds.size.height / 2.0f);
        self.activityView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:self.activityView];
        //[self.activityView setBackgroundColor:[UIColor blackColor]];
    }
    [self.activityView startAnimating];
    
    imageURL = [imageURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:[imageURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest * req = [NSMutableURLRequest requestWithURL:url];
    [req setHTTPMethod:@"GET"];
    
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        if (debug) {
            sleep(2);
        }
        NSURLResponse * res;
        NSError *error;
        NSData * data = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&error];
        UIImage * image = [UIImage imageWithData:data];
        dispatch_sync(dispatch_get_main_queue(), ^{
            //GRABANDO LA IMAGEN A LA CARPETA LOCAL
            NSString *nomImage = imageURL;
            nomImage = [nomImage reemplazarCaracteresCadena:URL_IMAGENES conNuevoCaracter:@""];
            nomImage = [nomImage reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
            [self setUp];
            if ([UIImage imageWithData:data]) {
                [self syncronizeImageLocal:data nameFile:nomImage];
                self.image = image;
            }else{
                //AQUI MODIFICAR LA IMAGEN QUE IRIA POR DEFAULT
                NSString * pathFilename = [[NSBundle mainBundle] pathForResource:@"img_placeholder" ofType:@"png"];
                NSData *data = [[NSData alloc]initWithContentsOfFile:pathFilename];
                [self syncronizeImageLocal:data nameFile:nomImage];
                self.image = [UIImage imageNamed:@"img_placeholder"];
            }
        });
    });
}

- (void)syncronizeImageLocal:(NSData *) data nameFile:(NSString *)nameFile
{
    NSString *rutaImagen = f(@"%@/%@",[self imagePath],nameFile);
    if([data  writeToFile:rutaImagen atomically:YES])
        [Soporte dump:@"Finalizado el copiado"];
}

- (NSString *)imagePath{
    NSArray *rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directorioDocumentos = [rutas objectAtIndex:0];
    NSString *rutaimages=[NSString stringWithFormat:@"%@/%@",directorioDocumentos,nombreCarpeta];
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:rutaimages]){
        [fileManagerM createDirectoryAtPath:rutaimages withIntermediateDirectories:YES attributes:nil error:nil];
        //NSLog(@"creando la carpeta");
    }else{
        //NSLog(@"la carpeta ya fue creada");
    }
    
    return rutaimages;
}

- (void)setUp{
    self.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
    [self.activityView removeFromSuperview];
    self.activityView = nil;
    [self.activityView stopAnimating];
}


@end
