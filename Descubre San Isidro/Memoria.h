//
//  Memoria.h
//  U Meet
//
//  Created by Luis on 17/01/16.
//  Copyright © 2016 lmarca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utiles.h"
#import "SyncronizeData.h"
#import "NSUserDefaults+Convenience.h"
#import "InfoUtilidadE.h"
#import "Constantes.h"

@interface Memoria : NSObject

+ (InfoUtilidadE *)getInfo;

+ (Memoria *) sharedInstance;
- (NSInteger )obtenerUserId;
- (BOOL)isEspanol;

@end
