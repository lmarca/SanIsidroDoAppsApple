//
//  DetalleActividadVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 4/04/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myActivity.h"
#import "Actividad.h"
#import "DynamicTableViewCell.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface DetalleActividadVC : myActivity<GMSMapViewDelegate,UIActionSheetDelegate,UIDocumentInteractionControllerDelegate>
{
    SLComposeViewController *mySLComposerSheet;
}

@property (retain) UIDocumentInteractionController * documentInteractionController;
@property(nonatomic,strong) Actividad *actividad;
@property(nonatomic,strong) NSMutableArray *entradasxactividad;
@property(nonatomic,strong) NSMutableArray *horariosxactividad;
@property(nonatomic,strong) NSMutableArray *textopresentacionxtipoactividad;

@property (strong, nonatomic) IBOutlet UITableView *tblDetalleActividad;

@end
