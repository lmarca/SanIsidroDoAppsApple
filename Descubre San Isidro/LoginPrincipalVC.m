//
//  LoginPrincipalVC.m
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import "LoginPrincipalVC.h"
#import "AppDelegate.h"

@interface LoginPrincipalVC ()<GIDSignInDelegate, GIDSignInUIDelegate> {
    IBOutlet UILabel *lblVerion;
}

@end

@implementation LoginPrincipalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
    [[GIDSignIn sharedInstance] signInSilently];
    
    lblVerion.text = @"Versión 1.2\nMunicipalidad de San Isidro\nLima - Perú";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onLoadLogin:(NSString *)nombre email:(NSString *)email{
    
    if ([Utiles isInternetAvailable]) {
        
        [self LoadingOn:NSLocalizedString(@"APP_LOADING", nil)];
        
        NSString *post = [NSString stringWithFormat:@"email=%@&name=%@&porRedSocial=true",email,nombre];
        
        [ConnectWS findAllObjectsWithQuery:f(@"%@security/%@",urlWS,@",/grabarusuario") query:post state:1 withBlock:^(id userInfo, NSError *error) {
            
            [self LoadingOff];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                           {
                               if (userInfo) {
                                   [Soporte dump:@"" prefijo:userInfo];
                                   
                                   NSDictionary *dictJson = (NSDictionary *)userInfo;
                                   
                                   if([dictJson[@"success"] boolValue]){
                                       [NSStandardUserDefaults saveObject:dictJson[@"ba_user_id"] forKey:ba_user_id];
                                       [NSStandardUserDefaults saveObject:dictJson[@"email"] forKey:email];
                                       [NSStandardUserDefaults saveObject:dictJson[@"name"] forKey:name];
                                       [NSStandardUserDefaults saveObject:dictJson[@"login_user"] forKey:login_user];
                                       [NSStandardUserDefaults saveBool:true forKey:Sesion];
                                       [NSStandardUserDefaults saveBool:true forKey:isRedesSociales];
                                       
                                   }
                               }
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   if ([NSStandardUserDefaults boolForKey:Sesion]) {
                                       
                                       AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                       [appDelegate loadTabBar];
                                   }
                               });
                           });
            
        }];
        
    }else [self SetAlerta:nil message:NSLocalizedString(@"APP_INTERNET_CURRENCY", nil)];
}

- (void)loginFacebook{
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email" forKey:@"fields"];
    FBSDKGraphRequest *graph = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters tokenString:[FBSDKAccessToken currentAccessToken].tokenString version:@"v2.3" HTTPMethod:nil];
    [graph startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"ResponseFB:%@",result);
        if (!error) {
            
            NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", result[@"id"]];
            NSLog(@"%@",userImageURL);
            [NSStandardUserDefaults saveObject:userImageURL forKey:@"userImageURL"];
            
            NSString *emailFb = @"";
            if (result[@"email"])
                emailFb = result[@"email"];
            
            [self onLoadLogin:result[@"name"] email:emailFb];
            
        }
        else NSLog(@"Error Log:%@",error.description);
    }];
    
}

#pragma mark - IBActions
- (IBAction)IniciarSesion:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    LoginVC *vc = (LoginVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self presentViewController:vc animated:TRUE completion:nil];
}

- (IBAction)irApp:(id)sender{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
                   {
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [Soporte dump:@"-- realizar algo aqui --"];
                           AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                           [appDelegate loadTabBar];
                       });
                   });
}

- (IBAction)btnFacebook:(id)sender{
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithReadPermissions:@[@"email", @"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Error Log:%@",error.description);
        } else if (result.isCancelled) {
            NSLog(@"Cancelado");
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
            }
            [self loginFacebook];
        }
    }];
}


- (void)presentSignInViewController:(UIViewController *)viewController{
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)btnGoogle:(id)sender{
    [self.signInButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)Register:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    RegisterVC *vc = (RegisterVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self presentViewController:vc animated:TRUE completion:nil];
}


- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if ([GIDSignIn sharedInstance].currentUser.authentication != nil) {
        
        // Perform any operations on signed in user here.
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
        
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSURL *imageURL = [user.profile imageURLWithDimension:300];
            [NSStandardUserDefaults saveObject:[imageURL absoluteString] forKey:@"userImageURL"];
        }
        [self onLoadLogin:user.profile.name email:user.profile.email];
    }
    
    [self toggleAuthUI];
}
// [END signin_handler]

// This callback is triggered after the disconnect call that revokes data
// access to the user's resources has completed.
// [START disconnect_handler]
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    NSLog(@"Disconnected user");
    [self toggleAuthUI];
}
// [END disconnect_handler]

// Signs the user out of the application for scenarios such as switching
// profiles.
// [START signout_tapped]
- (IBAction)didTapSignOut:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
    [self toggleAuthUI];
}
// [END signout_tapped]

// Note: Disconnect revokes access to user data and should only be called in
//     scenarios such as when the user deletes their account. More information
//     on revocation can be found here: https://goo.gl/Gx7oEG.
// [START disconnect_tapped]
- (IBAction)didTapDisconnect:(id)sender {
    [[GIDSignIn sharedInstance] disconnect];
}
// [END disconnect_tapped]

// [START toggle_auth]
- (void)toggleAuthUI {
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
        // Not signed in
        self.signOutButton.hidden = YES;
        self.disconnectButton.hidden = YES;
    } else {
        // Signed in
        self.signOutButton.hidden = NO;
        self.disconnectButton.hidden = NO;
    }
}


@end
