//
//  SanIsidroVC.h
//  SanIsidroApp
//
//  Created by Danny  Alva on 31/03/16.
//  Copyright © 2016 Danny  Alva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myActivity.h"
#import "AsyncImageView.h"
#import "DynamicTableViewCell.h"

@interface SanIsidroVC : myActivity <UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate>

@end
