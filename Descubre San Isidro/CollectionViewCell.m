//
//  CollectionViewCell.m
//  Descubre San Isidro
//
//  Created by Luis on 28/06/16.
//  Copyright © 2016 peru. All rights reserved.
//

#import "CollectionViewCell.h"
#import "Util_Color.h"

@implementation CollectionViewCell

@synthesize title, thumb, fondo;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    fondo.hidden = YES;
    
    //fondo.layer.cornerRadius = 55/2;
    //fondo.layer.borderColor = [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0].CGColor;
    //fondo.layer.borderWidth = 1;
    
    //fondo.backgroundColor = [UIColor colorWithHexString:@"6F9C4D" andAlpha:1.0];
    // Initialization code
}

-(void)llenarCelda:(MenuPortadaactividad *)menu{
    
    title.text = menu.txtnombre;
    
    NSString *img = menu.txticono;
    
    img = [img reemplazarCaracteresCadena:@"/" conNuevoCaracter:@""];
    
    if ([self verificarRutaImagenLocal:img]) {
        [thumb setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self imagePath],img]]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LOCAL"];
    }else{
        [thumb setImageURL:[NSString stringWithFormat:@"%@%@",URL_IMAGENES,menu.txticono]];
        [Soporte dump:@"CARGANDO LA IMAGEN DESDE LA WEB"];
    }
    
}

- (BOOL)verificarRutaImagenLocal:(NSString *)nombreImagen{
    NSString *tempR = [NSString stringWithFormat:@"%@/%@",[self imagePath],nombreImagen];
    BOOL result =[[NSFileManager defaultManager] fileExistsAtPath:tempR];
    return result;
}

- (NSString *)imagePath{
    NSArray *rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directorioDocumentos = [rutas objectAtIndex:0];
    NSString *rutaimages=[NSString stringWithFormat:@"%@/%@",directorioDocumentos,nombreCarpeta];
    NSFileManager *fileManagerM = [NSFileManager defaultManager];
    if (![fileManagerM fileExistsAtPath:rutaimages]){
        [fileManagerM createDirectoryAtPath:rutaimages withIntermediateDirectories:YES attributes:nil error:nil];
        //[Soporte dump:@"creando la carpeta"];
    }else{
        //[Soporte dump:@"la carpeta ya fue creada"];
    }
    
    return rutaimages;
}


@end
