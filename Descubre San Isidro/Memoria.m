//
//  Memoria.m
//  U Meet
//
//  Created by Luis on 17/01/16.
//  Copyright © 2016 lmarca. All rights reserved.
//

#import "Memoria.h"

@implementation Memoria

static InfoUtilidadE *info;

+ (Memoria *)sharedInstance {
    static Memoria *_sharedSingleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedSingleton = [[self alloc] init];
    });
    
    return _sharedSingleton;
}

- (id) init{
    if (self = [super init])
    {
        info = [InfoUtilidadE sharedInstance];
    }
    
    return self;
}

+ (InfoUtilidadE *)getInfo{
    if (info == NULL) {
        info = [InfoUtilidadE sharedInstance];
    }
    return info;
}

- (NSInteger )obtenerUserId {
    NSInteger Id = [[NSStandardUserDefaults stringForKey:@"ba_user_id"]integerValue];
    return Id == 0 ? -1 : Id;
}

- (BOOL)isEspanol {
    
    if ([[NSStandardUserDefaults stringForKey:kIdioma] rangeOfString:@"es"
                                                             options:NSRegularExpressionSearch|NSCaseInsensitiveSearch].location
        != NSNotFound)
        return true;
    
    return false;
}


@end
